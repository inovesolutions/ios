//
//  MenuTabBarController.swift
//  Olá Escola
//
//  Created by Rafael Marinheiro on 21/08/17.
//  Copyright © 2017 Bmtech Soluções Tecnológicas. All rights reserved.
//

import UIKit
import Firebase


class MenuTabBarController: UITabBarController, MessagingDelegate, BadgesUpdateProtocol{
    
    var messagesBadge : Int = 0
    var chatsBadge: Int = 0
    
    func messagesUpdated() {
        messagesBadge = 0
        fb.db.me.messages.forEach { (message) in
            if(!message.read!){
                messagesBadge = messagesBadge + 1
            }
        }
        
        if(messagesBadge > 0){
            self.tabBar.items![0].badgeValue = String(messagesBadge)
        }
        else{
            self.tabBar.items![0].badgeValue = nil
        }
    }
    
    func chatsUpdated(chatId: String) {
        chatsBadge = 0
        let chat = fb.db.me.chats.first { (c) -> Bool in
            return c.contactId == chatId
        }
        chat?.messages.forEach { (message) in
            if(!message.read && !message.senderId.elementsEqual(fb.db.getUserId())){
                chatsBadge = chatsBadge + 1
            }
        }
        
        if(chatsBadge > 0){
            self.tabBar.items![1].badgeValue = String(chatsBadge)
        }
        else{
            self.tabBar.items![1].badgeValue = nil
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        fb.db.badgesUpdateProtocol = self
    }
 // Do any additional setup after loading the view.
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        fb.db.updateMessagingToken(token: fcmToken)
    }

}
