//
//  fb.swift
//  Olá Escola
//
//  Created by Rafael Marinheiro on 24/08/17.
//  Copyright © 2017 Bmtech Soluções Tecnológicas. All rights reserved.
//

import Foundation
import Firebase


class fb{
    
    private var connected : Bool = false
    static var db = fb()
    var me : User = User()
    
    
    // Referencias
    private var rootReference : DatabaseReference = Database.database().reference()
    private var messagesReference : DatabaseReference = Database.database().reference()
    private var chatsReference : DatabaseReference = Database.database().reference()
    private var profileReference : DatabaseReference = Database.database().reference()
    private var contactsReference : DatabaseReference = Database.database().reference()
    private var relationsReference : DatabaseReference = Database.database().reference()
    private var tagsReference : DatabaseReference = Database.database().reference()

    // Snapshot
    private var rootSnapshot : DataSnapshot? = nil
    private var messagesSnapshot : DataSnapshot? = nil
    private var chatsSnapshot : DataSnapshot? = nil
    private var profileSnapshot : DataSnapshot? = nil
    private var contactsSnapshot : DataSnapshot? = nil
    private var relationsSnapshot : DataSnapshot? = nil
    private var tagsSnapshot : DataSnapshot? = nil

    // Observers
    private var rootHandle : UInt? = nil
    private var messagesHandle : UInt? = nil
    private var messagesAddedHandle : UInt? = nil
    private var messagesChangedHandle : UInt? = nil
    private var messagesRemovedHandle : UInt? = nil
    private var chatsHandle : UInt? = nil
    private var chatsAddedHandle : UInt? = nil
    private var chatsChangedHandle : UInt? = nil
    private var chatsRemovedHandle : UInt? = nil
    private var profileHandle : UInt? = nil
    private var profileAddedHandle : UInt? = nil
    private var profileChangedHandle : UInt? = nil
    private var profileRemovedHandle : UInt? = nil
    private var contactsHandle : UInt? = nil
    private var contactsAddedHandle : UInt? = nil
    private var contactsChangedHandle : UInt? = nil
    private var contactsRemovedHandle : UInt? = nil
    private var relationsHandle : UInt? = nil
    private var relationsAddedHandle : UInt? = nil
    private var relationsChangedHandle : UInt? = nil
    private var relationsRemovedHandle : UInt? = nil
    private var tagsHandle : UInt? = nil
    private var tagsAddedHandle : UInt? = nil
    private var tagsChangedHandle : UInt? = nil
    private var tagsRemovedHandle : UInt? = nil
    private var authHandle : AuthStateDidChangeListenerHandle? = nil
    
    // Protocolos
    var userProtocol : UsersProtocol? = nil
    var messsageProtocol : MessagesProtocol? = nil
    var chatProtocol : ChatsProtocol? = nil
    var profileProtocol : ProfileProtocol? = nil
    var contactsProtocol : ContactsProtocol? = nil
    var relationsProtocol : RelationsProtocol? = nil
    var tagsProtocol : TagsProtocol? = nil
    var authProtocol : AuthProtocol? = nil
    var badgesUpdateProtocol : BadgesUpdateProtocol? = nil
    
    var presence : [String:Bool] = [:]

    // Construtor
    private init(){

    }
    
    public func isConnected() -> Bool{
        return self.connected
    }
    
    public func getUserId() -> String{
        return self.me.userId
    }


    public func connect() {
        
        if Auth.auth().currentUser == nil {
            self.authProtocol?.authRequire()
            self.connected = false
            return
        }
        // Limpa o objeto
        clearMe()
        
        self.me.userId = Auth.auth().currentUser!.uid
        self.me.profile = Profile()
        self.me.profile.profileId = self.me.userId
        self.rootReference = Database.database().reference().child("users").child(self.me.userId as String)
        self.messagesReference = rootReference.child("messages")
        self.chatsReference = rootReference.child("chats")
        self.profileReference = rootReference.child("profile")
        self.contactsReference = rootReference.child("contacts")
        self.relationsReference = rootReference.child("relations")
        self.tagsReference = rootReference.child("tags")
        self.observerAll()

        self.authHandle = Auth.auth().addStateDidChangeListener({ (auth, user) in
            if auth.currentUser == nil {
                self.authProtocol?.authRequire()
            }
        })
        
        let token = Messaging.messaging().fcmToken
        self.updateMessagingToken(token: token)
        
        self.connected = true
        
    }

    public func disconnect(){
        self.me.userId = "" // PENDENCIA -- Implementar diretamente da autenticacao
        removeAllObservers()
        if self.authHandle != nil {
            Auth.auth().removeStateDidChangeListener(self.authHandle!)
        }
        connected = false
    }
    
    public func clearMe(){
        self.me = User()
    }
    
    public func logout() {
        if Auth.auth().currentUser != nil {
            try! Auth.auth().signOut()
        }
    }

    
    private func observeConnected() {
        let presenceRef = Database.database().reference().child("presence").child(me.userId)
        let connectedRef = Database.database().reference(withPath: ".info/connected")
        connectedRef.observe(.value, with: { snapshot in
            if snapshot.value as? Bool ?? false {
                presenceRef.child("connected").setValue(true)
                presenceRef.child("name").setValue(self.me.profile.displayName)
                presenceRef.child("timestamp").setValue(Firebase.ServerValue.timestamp())
            }
        })
        
        presenceRef.child("connected").onDisconnectSetValue(false)
        presenceRef.child("timestamp").onDisconnectSetValue(Firebase.ServerValue.timestamp())
    }
    
    func observerAll() {
        observeProfile()
        observeRelations()
        observeContacts()
        observeTags()
        observeMessages()
        observeChats()
        observeConnected()
    }
    
    public func removeAllObservers(){
        Database.database().reference().removeAllObservers()
    }

    // MESSAGES - BEGIN
    /**********************************************************************/
    func observeMessages() {
        
        self.messagesAddedHandle = messagesReference.observe(.childAdded, with: { (DataSnapshot) in
            self.addedMessage(DataSnapshot)
        }){ (error) in
            print("Erro: (ObserveMessages().messagesAdded: ",error.localizedDescription)
        }

        self.messagesChangedHandle = messagesReference.observe(.childChanged, with: { (DataSnapshot) in
            self.changedMessage(DataSnapshot)
        }){ (error) in
            print("Erro: (ObserveMessages().messagesChanged: ",error.localizedDescription)
        }
        
        self.messagesRemovedHandle = messagesReference.observe(.childRemoved, with: { (DataSnapshot) in
            self.removedMessage(key: DataSnapshot.key)
        }){ (error) in
            print("Erro: (ObserveMessages().messagesRemoved: ",error.localizedDescription)
        }
    }
    
    func addedMessage(_ messageDataSnapshot: DataSnapshot){
        
        let message = Message()
        message.fill(messageDataSnapshot.key, (messageDataSnapshot.value as? [String: Any])!)
        if message.deleted == false {
            fb.db.me.messages.append(message)
            updateMessageRelations(message)
            if self.messsageProtocol != nil{
                self.messsageProtocol!.onAdded()
            }
            if message.received == false {
                checkReceivedMessage(message: message)
            }
        }
        
        if(self.badgesUpdateProtocol != nil){
            self.badgesUpdateProtocol?.messagesUpdated()
        }
        
        fb.db.me.messages = fb.db.me.messages.sorted(by: {$0.messageId > $1.messageId})
    }
    
    func changedMessage(_ messageDataSnapshot: DataSnapshot){
        
        let message = Message()
        message.fill(messageDataSnapshot.key , messageDataSnapshot.value as? [String:Any] ?? [:])
        
        let index = fb.db.me.messages.index(where: { (currentMessage) -> Bool in
            return currentMessage.messageId == message.messageId
        })
        
        if index != nil {
            if message.deleted == true {
                fb.db.me.messages.remove(at: index!)
            }else{
                fb.db.me.messages[index!] = message
                updateMessageRelations(message)
            }
            if self.messsageProtocol != nil{
                self.messsageProtocol!.onChanged()
            }
            
        }else{
            addedMessage(messageDataSnapshot)
        }
        
        if(self.badgesUpdateProtocol != nil){
            self.badgesUpdateProtocol?.messagesUpdated()
        }
        
        fb.db.me.messages = fb.db.me.messages.sorted(by: {$0.messageId > $1.messageId})
    }
    
    func updateMessageRelations(_ message: Message){
        
        if message.content.header.relationUsers.count > 0  {
            for relation in fb.db.me.relations{
                let relationId = relation.relationId
                if message.content.header.relationUsers.contains(relationId){
                    message.content.header.relations.append(relation)
                }
            }
            messagesReference.child(message.messageId).child("content").child("header").child("relations").setValue(message.content.header.relations, withCompletionBlock: { (error, DatabaseReference) in
                if error != nil {
                    print("Erro: \(String(describing: error?.localizedDescription))")
                }
            })
        }
    }

    
    func removedMessage(key: String){
        let index = fb.db.me.messages.index(where: { (currentMessage) -> Bool in
            return currentMessage.messageId  == key
        })
        
        if index != nil {
            fb.db.me.messages.remove(at: index!)
            if self.messsageProtocol != nil{
                self.messsageProtocol!.onRemoved()
            }
        }else{
            print("Erro: Foi excluida uma mensagem que ainda nao estava na base (messageKey: \(key))")
        }
        
        if(self.badgesUpdateProtocol != nil){
            self.badgesUpdateProtocol?.messagesUpdated()
        }
    }

    func checkDeleteMessage(message: Message){
        messagesReference.child(message.messageId).child("deleted").setValue(true)
        print("FALTA IMPLEMENTAR O ERRO DE GRAVACAO DELETED") // FALTA IMPLEMENTAR O ERRO DE GRAVACAO
    }

    func checkReadMessage(message: Message){
        messagesReference.child(message.messageId).child("read").setValue(true)
        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber - 1
        print("FALTA IMPLEMENTAR O ERRO DE GRAVACAO - READ") // FALTA IMPLEMENTAR O ERRO DE GRAVACAO
    }
    
    func checkReceivedMessage(message: Message){
        messagesReference.child(message.messageId).child("received").setValue(true)
        print("FALTA IMPLEMENTAR O ERRO DE GRAVACAO - RECEIVED") // FALTA IMPLEMENTAR O ERRO DE GRAVACAO
    }
    
    
    // CHATS
    /**********************************************************************/
 
 
 
    func observeChats() {
        
        self.chatsAddedHandle = chatsReference.observe(.childAdded, with: { (DataSnapshot) in
            self.addChat(DataSnapshot)
        })
        
        self.chatsChangedHandle = chatsReference.observe(.childChanged, with: { (DataSnapshot) in
            self.updateChat(DataSnapshot)
        })
        
        self.chatsRemovedHandle = chatsReference.observe(.childRemoved, with: { (DataSnapshot) in
            self.deleteChat(DataSnapshot)
        })
    }

    func addChat(_ chatSnapshot: DataSnapshot) {
        let chat = Chat()
        chat.fill(snap: chatSnapshot)
        self.me.chats.append(chat)
        if self.chatProtocol != nil{
            self.chatProtocol?.onAdded(chatSnapshot.key)
        }
        
        if(self.badgesUpdateProtocol != nil){
            self.badgesUpdateProtocol?.chatsUpdated(chatId: chatSnapshot.key)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(3000)) {
            self.cancelWriting(chatId: chat.contactId)
        }
        
        self.me.chats = self.me.chats.sorted(by: {$0.lastChange > $1.lastChange})
        
        Database.database().reference().child("presence").child(chat.contactId)
            .observe(.value, with: { (snapshot) in
                if(snapshot.exists() && snapshot.childSnapshot(forPath: "connected").value as! Bool == true){
                    self.presence[chat.contactId] = true
                } else {
                    self.presence[chat.contactId] = false
                }
                
                if self.chatProtocol != nil{
                    self.chatProtocol?.onAdded(chatSnapshot.key)
                }
            })
    }
    
    func updateChat(_ chatSnapshot: DataSnapshot){
        
        let index = fb.db.me.chats.index(where: { (currentChat) -> Bool in
            return currentChat.contactId == chatSnapshot.key
        })
        
        if index != nil {
            
            fb.db.me.chats[index!].fill(snap: chatSnapshot)
            
            if fb.db.me.chats[index!].messages.count == 0 {
                fb.db.me.chats.remove(at: index!)
            }
            
            if self.chatProtocol != nil{
                self.chatProtocol!.onChanged(chatSnapshot.key)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(3000)) {
                self.cancelWriting(chatId: fb.db.me.chats[index!].contactId)
            }
        }
        
        self.me.chats = self.me.chats.sorted(by: {$0.lastChange > $1.lastChange})
        
        if(self.badgesUpdateProtocol != nil){
            self.badgesUpdateProtocol?.chatsUpdated(chatId: chatSnapshot.key)
        }
    }
    
    func cancelWriting(chatId: String){
        Database.database().reference().child("users")
        .child(me.userId).child("chats").child(chatId).child("writing").setValue(false)
    }
    
    func setWriting(chatId: String){
        Database.database().reference().child("users")
            .child(chatId).child("chats").child(me.userId).child("writing").setValue(true)
    }
    
    func getChatById(chatId: String) -> Chat?{
        return fb.db.me.chats.first(where: { (chat) -> Bool in
            return chat.contactId == chatId
        })
    }
    
    func deleteChat(_ chatSnapshot: DataSnapshot){
        
        let index = fb.db.me.chats.index(where: { (currentChat) -> Bool in
            return currentChat.contactId == chatSnapshot.key
        })
        if index != nil {
            fb.db.me.chats.remove(at: index!)
        }
        if self.chatProtocol != nil{
            self.chatProtocol!.onRemoved(chatSnapshot.key)
        }
        if(self.badgesUpdateProtocol != nil){
            self.badgesUpdateProtocol?.chatsUpdated(chatId: chatSnapshot.key)
        }
    }
   
    func sendChatMessage(contactId: String, type: MessageType, content: String) {
        
        let message = Chat.Message()
        let userId = fb.db.getUserId()
        
        if type == .text {
            message.type = "text"
        }else if type == .photo {
            message.type = "image"
        }
        message.content = content
        message.sent = true
        message.senderId = userId
        
        let key = Database.database().reference().child("users").child("chats").child(contactId).child("messages").childByAutoId().key
        var messageDict = message.toDict()
        messageDict["date"] = [".sv":"timestamp"]
        
        let contact = self.me.contacts.first { (c) -> Bool in
            return c.profileId == contactId
        }
        
        let profile: Profile
        
        if(contact != nil){
            profile = contact!
        }
        else {
            profile = self.me.chats.first(where: { (c) -> Bool in
                return c.contactId == contactId
            })!.profile
        }
        
        let updates = ["/users/\(userId)/chats/\(contactId)/messages/\(key)" : messageDict,
                       "/users/\(userId)/chats/\(contactId)/lastChange" : [".sv":"timestamp"],
                       "/users/\(userId)/chats/\(contactId)/profile" : profile.toDict(),
                       "/users/\(contactId)/chats/\(userId)/messages/\(key)" : messageDict,
                       "/users/\(contactId)/chats/\(userId)/lastChange" : [".sv":"timestamp"],
                       "/users/\(contactId)/chats/\(userId)/profile" : self.me.profile.toDict()]

        // Adiciona no objeto local
        Database.database().reference().updateChildValues(updates) { (error, reference) in
            if error != nil {
                print(error.debugDescription)
            }
        }
    }
    
    func checkChatMessageRead(contactId: String, message: Chat.Message) {
        if message.type == "image"  {
            if !message.uploaded {
                return
            }
            if !fb.db.chatLocalFileExists(message, contactId){
                return
            }
        }

        if !message.received {
            self.checkChatMessageReceived(contactId: contactId, message: message)
        }
        
        let meId = getUserId()
        let updates = ["/users/\(meId)/chats/\(contactId)/messages/\(message.messageId)/read" : true,
                       "/users/\(contactId)/chats/\(meId)/messages/\(message.messageId)/read" : true]
        
        // Adiciona no objeto local
        Database.database().reference().updateChildValues(updates) { (error, reference) in
            if error != nil {
                print(error.debugDescription)
            }
        }
        
        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber - 1
        
        print("FALTA IMPLEMENTAR O ERRO DE GRAVACAO - MESSAGE READ") // FALTA IMPLEMENTAR O ERRO DE GRAVACAO
    }

    func checkChatMessageReceived(contactId: String, message: Chat.Message){
        if message.type == "image"  {
            if !message.uploaded {
                return
            }
        }

        let meId = getUserId()
        let updates = ["/users/\(meId)/chats/\(contactId)/messages/\(message.messageId)/received" : true,
                       "/users/\(contactId)/chats/\(meId)/messages/\(message.messageId)/received" : true]
                       
        // Adiciona no objeto local
        Database.database().reference().updateChildValues(updates) { (error, reference) in
            if error != nil {
                print(error.debugDescription)
            }
        }
        print("FALTA IMPLEMENTAR O ERRO DE GRAVACAO - MESSAGE RECEIVED") // FALTA IMPLEMENTAR O ERRO DE GRAVACAO
    }

    
    
    // PROFILE
    /**********************************************************************/

    func observeProfile() {

        self.profileAddedHandle = profileReference.observe(.childAdded, with: { (DataSnapshot) in
            self.setItemProfile(DataSnapshot)
        }){ (error) in
            print("Erro: (observeProfile().profileAdded: ",error.localizedDescription)
        }

        self.profileChangedHandle = profileReference.observe(.childChanged, with: { (DataSnapshot) in
            self.setItemProfile(DataSnapshot)
        }){ (error) in
            print("Erro: (observeProfile().profileChanged: ",error.localizedDescription)
        }
        
        self.profileRemovedHandle = profileReference.observe(.childRemoved, with: { (DataSnapshot) in
            self.setItemProfile(DataSnapshot)
        }){ (error) in
            print("Erro: (observeProfile().profileChanged: ",error.localizedDescription)
        }
    }

    
    
    func setItemProfile(_ snap: DataSnapshot){
    
        switch snap.key {
        case "displayName":
            fb.db.me.profile.displayName = snap.value as? String ?? ""
            break
        case "displaySubtitle":
            fb.db.me.profile.displaySubtitle = snap.value as? String ?? ""
            break
        case "avatar":
            fb.db.me.profile.avatar = snap.value as? String ?? ""
            break
        case "email":
            fb.db.me.profile.email = snap.value as? String ?? ""
            break
        case "mobilePhone":
            fb.db.me.profile.mobilePhone = snap.value as? String ?? ""
            break
        case "gender":
            fb.db.me.profile.gender = snap.value as? String ?? ""
            break
        case "type":
            fb.db.me.profile.type = snap.value as? String ?? ""
            break
        default:
            break
        }
        profileProtocol?.onChangedProfile()
    }
    
    
    // RELATIONS
    /**********************************************************************/
    
    func observeRelations() {
        
        self.relationsAddedHandle = relationsReference.observe(.value, with: { (DataSnapshot) in
            self.setValueRelation(DataSnapshot)
            self.relationsProtocol?.onChanged()
        }){ (error) in
            print("Erro: (observeRelations().addedRelation: ",error.localizedDescription)
        }

    }

    func setValueRelation(_ snap: DataSnapshot){
        fb.db.me.relations.removeAll()
        for (key, value) in snap.value as? [String:Any] ?? [:] {
            let relation = Relation()
            relation.relationId = key as String
            relation.fill( key: key, dict: (value as? [String: Any] )!)
            fb.db.me.relations.append(relation)
        }
    }
    
    
    // CONTACTS
    /**********************************************************************/
    
    func observeContacts() {
        
        self.contactsAddedHandle = contactsReference.observe(.childAdded, with: { (DataSnapshot) in
            self.addContact(DataSnapshot)
        })
        
        self.contactsChangedHandle = contactsReference.observe(.childChanged, with: { (DataSnapshot) in
            if self.contactsProtocol != nil{
                self.contactsProtocol!.onChanged()
            }
        })
        self.contactsRemovedHandle = contactsReference.observe(.childRemoved, with: { (DataSnapshot) in
            if self.contactsProtocol != nil{
                self.contactsProtocol!.onRemoved()
            }
        })
    }

    
    
    func addContact(_ contactSnapshot: DataSnapshot) {
        let contact = Contact()
        contact.fill(contactSnapshot.key, (contactSnapshot.value as? [String: Any])! )
        self.me.contacts.append(contact)
        if self.contactsProtocol != nil{
            self.contactsProtocol?.onAdded()
        }
    }
    
    /*
 
 
    func updateChat(_ chatSnapshot: DataSnapshot){
        
        let index = fb.db.me.chats.index(where: { (currentChat) -> Bool in
            return currentChat.contactId == chatSnapshot.key
        })
        
        if index != nil {
            
            fb.db.me.chats[index!].fill(snap: chatSnapshot)
            
            if fb.db.me.chats[index!].messages.count == 0 {
                fb.db.me.chats.remove(at: index!)
            }
            
            if self.chatProtocol != nil{
                self.chatProtocol!.onChanged(chatSnapshot.key)
            }
        }
    }
    
    
    func deleteChat(_ chatSnapshot: DataSnapshot){
        
        let index = fb.db.me.chats.index(where: { (currentChat) -> Bool in
            return currentChat.contactId == chatSnapshot.key
        })
        if index != nil {
            fb.db.me.chats.remove(at: index!)
        }
        if self.chatProtocol != nil{
            self.chatProtocol!.onRemoved(chatSnapshot.key)
        }
    }
    */
    
    
    // TAGS
    /**********************************************************************/

    func observeTags() {
        
        self.tagsAddedHandle = tagsReference.observe(.childAdded, with: { (DataSnapshot) in
            if self.tagsProtocol != nil{
                self.tagsProtocol!.onAdded()
            }
        })
        self.tagsChangedHandle = tagsReference.observe(.childChanged, with: { (DataSnapshot) in
            if self.tagsProtocol != nil{
                self.tagsProtocol!.onChanged()
            }
        })
        self.tagsRemovedHandle = tagsReference.observe(.childRemoved, with: { (DataSnapshot) in
            if self.tagsProtocol != nil{
                self.tagsProtocol!.onRemoved()
            }
        })
            
    }
    
    
    // FIREBASE STORAGE FUNCTIONS
    /**********************************************************************/
    
    
    var storageImageDownloadProtocol : StorageImageDownloadProtocol? = nil
    var storageAvatarDownloadProtocol : StorageAvatarUpdateProtocol? = nil
    var chatDownloadProtocol : StorageChatDownloadProtocol? = nil
    
    // AVATAR
    func storageGetImageAvatar(_ profile: Profile) -> UIImage?{
        
        let avatarImageReference = Storage.storage().reference().child("avatars").child(profile.profileId).child(profile.avatar)
        var localFileUrl = FileManager.default.urls(for: .documentDirectory, in:.userDomainMask).first!
        
        localFileUrl.appendPathComponent("avatars", isDirectory: true)
        localFileUrl.appendPathComponent(profile.profileId , isDirectory: true)
        localFileUrl.appendPathComponent(profile.avatar)
        
        if let imageData = NSData(contentsOf: localFileUrl) {
            return UIImage(data: imageData as Data) 
        }else{
            let downloadTask = avatarImageReference.write(toFile: localFileUrl)
            downloadTask.observe(.failure) { (snapshot) in
                if snapshot.error != nil {
                    print(snapshot.error!.localizedDescription)
                    print(snapshot.error.debugDescription)
                    return
                }
            }
            downloadTask.observe(.success) { (snapshot) in
                print("Download completed (AVATAR):  \(profile.avatar)")
                self.storageAvatarDownloadProtocol!.onUpdateAvatar()
            }
            return nil
        }
    }
    
    func storageGetImageAvatar(_ profile: Profile, completion: @escaping (_ image: UIImage)->()){
        
        let avatarImageReference = Storage.storage().reference().child("avatars").child(profile.profileId).child(profile.avatar)
        var localFileUrl = FileManager.default.urls(for: .documentDirectory, in:.userDomainMask).first!
        
        localFileUrl.appendPathComponent("avatars", isDirectory: true)
        localFileUrl.appendPathComponent(profile.profileId , isDirectory: true)
        localFileUrl.appendPathComponent(profile.avatar)
        
        if let imageData = NSData(contentsOf: localFileUrl) {
            completion(UIImage(data: imageData as Data)!)
        }else{
            let downloadTask = avatarImageReference.write(toFile: localFileUrl)
            downloadTask.observe(.failure) { (snapshot) in
                if snapshot.error != nil {
                    print(snapshot.error!.localizedDescription)
                    print(snapshot.error.debugDescription)
                    return
                }
            }
            downloadTask.observe(.success) { (snapshot) in
                print("Download completed (AVATAR):  \(profile.avatar)")
                if let imageData = NSData(contentsOf: localFileUrl) {
                    completion(UIImage(data: imageData as Data)!)
                }
            }
        }
    }
    
    
    
    // CHATS
    
    func chatLocalFile(_ message : Chat.Message, _ contactId : String) -> URL?{
        var localFileUrl = FileManager.default.urls(for: .documentDirectory, in:.userDomainMask).first!
        localFileUrl.appendPathComponent("chats", isDirectory: true)
        localFileUrl.appendPathComponent(contactId, isDirectory: true)
        localFileUrl.appendPathComponent(message.messageId, isDirectory: true)
        localFileUrl.appendPathComponent(message.content)
        return localFileUrl
    }

    func chatLocalFileExists(_ message : Chat.Message, _ contactId : String) -> Bool{

        if let localFileUrl = chatLocalFile(message, contactId) {
            if (FileManager.default.fileExists(atPath: localFileUrl.path)) {
                return true
            }
        }
        return false
    }
    
    func chatDownloadToLocalFile(_ message: Chat.Message, _ contactId: String) -> Bool{
        
        var chatImageReference : StorageReference
        
        if message.senderId != self.getUserId() {
            chatImageReference = Storage.storage().reference().child("users").child(contactId).child("chats").child(self.getUserId()).child("messages").child(message.messageId).child(message.content)
        }else{
            chatImageReference = Storage.storage().reference().child("users").child(self.getUserId()).child("chats").child(contactId).child("messages").child(message.messageId).child(message.content)

        }
        
        // se o arquivo já existir retorna true
        if chatLocalFileExists(message, contactId) {
            return true
        }
        
        if let localFileUrl = chatLocalFile(message, contactId){
            let downloadTask = chatImageReference.write(toFile: localFileUrl)
            downloadTask.observe(.failure) { (snapshot) in
                if snapshot.error != nil {
                    if self.chatDownloadProtocol != nil{
                        self.chatDownloadProtocol!.onFail(message: message, error: snapshot.error!)
                    }
                    print("Download file message \(message.messageId) ERROR :")
                    print("LOCALIZED DESCRIPTION: \(snapshot.error!.localizedDescription)")
                    print("DEBUG DESCRIPTION: \(snapshot.error.debugDescription)")
                    return
                }
            }
            downloadTask.observe(.success) { (snapshot) in
                print("Download completed (CHAT IMAGE MESSAGE):  \(message.messageId)")
                if self.chatDownloadProtocol != nil{
                    self.chatDownloadProtocol!.onSuccess(message: message)
                }
                print("Download file message \(message.messageId) SUCCESS!")
                return
            }
            
            downloadTask.observe(.progress, handler: { (snapshot) in
                let percentComplete = (snapshot.progress?.fractionCompleted)! * 100.0
                if self.chatDownloadProtocol != nil{
                    self.chatDownloadProtocol!.onProgress(message: message, completed: Float(percentComplete))
                }
            })
            
            downloadTask.observe(.pause, handler: { (snapshot) in
                if self.chatDownloadProtocol != nil{
                    self.chatDownloadProtocol!.onResume(message: message)
                }
                print("Download file message \(message.messageId) PAUSED!")
            })
            
            downloadTask.observe(.resume, handler: { (snapshot) in
                if self.chatDownloadProtocol != nil{
                    self.chatDownloadProtocol!.onPause(message: message)
                    print("Download file message \(message.messageId) RESUMED!")
                }
            })
            return true
        }
        return false
    }

    func chatGetLocalFile(_ message: Chat.Message, _ contactId: String) -> UIImage?{
     
        if chatLocalFileExists(message, contactId) {
            if let localFileUrl = chatLocalFile(message, contactId){
                if (FileManager.default.fileExists(atPath: localFileUrl.path)) {
                    let imageData = NSData(contentsOf: localFileUrl)
                    return UIImage(data: imageData! as Data)!
                }
            }
        }
        return nil
    }
    

    // MESSAGES
    func storageGetImage(_ fileName: String){
        
        var imageReference : StorageReference {
            return Storage.storage().reference().child("messages")

        }
        
        let downloadImageReference = imageReference.child(fileName)
        
        var localFileUrl = FileManager.default.urls(for: .documentDirectory, in:.userDomainMask).first!
        localFileUrl.appendPathComponent("images", isDirectory: true)
        localFileUrl.appendPathComponent(fileName)
        
        // Observe changes in status
        let downloadTask = downloadImageReference.write(toFile: localFileUrl)
        
        
        downloadTask.observe(.failure) { (snapshot) in
            if snapshot.error != nil {
                print(snapshot.error!.localizedDescription)
                print(snapshot.error.debugDescription)
                self.storageImageDownloadProtocol!.onFail(error: snapshot.error!)
                return
            }
        }
        
        downloadTask.observe(.pause) { (snapshot) in
            print("Download paused:  \(fileName)")
        }
        
        downloadTask.observe(.progress) { (snapshot) in
            let percentComplete = (snapshot.progress?.fractionCompleted)! * 100.0
            print("\(percentComplete)")
            self.storageImageDownloadProtocol!.onProgress(completed: Float(percentComplete))
        }
        
        downloadTask.observe(.resume) { (snapshot) in
            print("Download resumed:  \(fileName)")
        }
        
        downloadTask.observe(.success) { (snapshot) in
            print("Download completed:  \(fileName)")
            self.storageImageDownloadProtocol!.onSuccess()
        }
        
    }
    
    func updateMessagingToken(token: String?){
        if (token == nil)  { return }
        let meId = getUserId()
        let updates : [String:String] = ["/users/\(meId)/profile/pushId" : token!,
                                         "/users/\(meId)/pushId" : token!]
        Database.database().reference().updateChildValues(updates) { (error, reference) in
            if error != nil {
            print(error.debugDescription)
            }
        }
    }
}

    
