//
//  ChatContactListTableViewController.swift
//  OlaEscola
//
//  Created by Rafael Marinheiro on 06/12/17.
//  Copyright © 2017 Bmtech Soluções Tecnológicas. All rights reserved.
//

import UIKit

class ChatContactCell : UITableViewCell{
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var photo: UIImageView!
    
}

class ChatContactListTableViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.title = "Contatos"
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return fb.db.me.contacts.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> ChatContactCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatContactCell", for: indexPath) as! ChatContactCell
        
        var iconAvatar = ""
        if(fb.db.me.contacts[indexPath.row].gender == "male"){
            iconAvatar = "IconeAvatarMOn"
        }
        else {
            iconAvatar = "IconeAvatarFOn"
        }
        cell.photo.image = UIImage.init(named: iconAvatar)
        fb.db.storageGetImageAvatar(fb.db.me.contacts[indexPath.row]){ (image) -> () in
            cell.photo.image = image
        }
        cell.photo.layer.borderWidth = 1.0
        cell.photo.layer.borderColor = UIColor.lightGray.cgColor
        cell.photo.layer.cornerRadius = cell.photo.frame.size.width / 2
        cell.photo.clipsToBounds = true
        cell.name.text = fb.db.me.contacts[indexPath.row].displayName
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let selectedIndex = self.tableView.indexPathForSelectedRow {
            if segue.identifier == "contactToChatSegue"{
                if let destinationVC = segue.destination as? ChatVC{
                    let contact : Contact = fb.db.me.contacts[selectedIndex.row] as Contact
                    var chat = fb.db.getChatById(chatId: contact.profileId)
                    if(chat == nil){
                        chat = Chat()
                        chat?.contactId = contact.profileId
                        chat?.lastChange = 0
                        chat?.profile = contact
                        chat?.writing = false
                        chat?.messages = Array()
                    }
                    
                    destinationVC.chat = chat!
                    destinationVC.contactId = contact.profileId
                }
            }
        }
        
    }

}
