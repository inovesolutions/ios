//
//  ChatDetailImageViewController.swift
//  OlaEscola
//
//  Created by Rafael Marinheiro on 06/12/17.
//  Copyright © 2017 Bmtech Soluções Tecnológicas. All rights reserved.
//

import UIKit

class ChatDetailImageViewController: UIViewController {

    @IBOutlet weak var imageScrollView: ImageScrollView!
    @IBOutlet weak var SubtitleText: UITextView!
    @IBAction func BackButtonTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    var message : Chat.Message = Chat.Message()
    var contactId : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        if fb.db.chatLocalFileExists(self.message, self.contactId) {
            let localfileUrl = fb.db.chatLocalFile(self.message, self.contactId)
            let imageData = NSData(contentsOf: localfileUrl!)
            if let photo = UIImage(data: imageData! as Data) {
                self.imageScrollView.display(image: photo)
            }
            //self.ImagePreview.image =
            self.SubtitleText.text = self.message.subtitle
        }else{
            print("Não foi possível carregar a imagem, faça o download primeiro!")
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
