//
//  SecondViewController.swift
//  Olá Escola
//
//  Created by Rafael Marinheiro on 09/08/17.
//  Copyright © 2017 Bmtech Soluções Tecnológicas. All rights reserved.
//

import UIKit
import Firebase

class ChatsTableViewCell : TDBadgedCell{
    
    @IBOutlet weak var AvatarImageView: UIImageView!
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var ResumeLabel: UILabel!
    @IBOutlet weak var DateTimeLabel: UILabel!
    @IBOutlet weak var BadgeLabel: UILabel!
    @IBOutlet weak var IconResumeLabel: UILabel!
    
}

class ChatsTableViewController: UITableViewController, ChatsProtocol, StorageImageDownloadProtocol, StorageAvatarUpdateProtocol{

    @IBAction func ComposeChatButton(_ sender: Any) {
        self.alert(message: "Olá com esse botao você adiciona um novo chat")
    }
    @IBOutlet weak var ComposeChatButton: UIBarButtonItem!
    
    // CHAT Protocol
    func onAdded(_ chatId:String) {
        tableView.reloadData()
    }
    func onChanged(_ chatId:String) {
        tableView.reloadData()
    }
    func onRemoved(_ chatId:String) {
        tableView.reloadData()
    }
  
    
    // STORAGE Protocol
    func onPause() {    }
    func onResume() {    }
    func onSuccess() {    }
    func onFail(error: Error) {    }
    func onProgress(completed: Float) {    }
    
    // AVATAR Protocol
    func onUpdateAvatar() {  tableView.reloadData()  }
    
    
    override func viewDidLoad() {
        self.tableView.tableFooterView = UIView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        fb.db.chatProtocol = self
        fb.db.storageImageDownloadProtocol = self
        fb.db.storageAvatarDownloadProtocol = self
        tableView.reloadData()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // controla quantas linhas mostra a tabela
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fb.db.me.chats.count
    }
    
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1 // núemro de divisores
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> ChatsTableViewCell {
        
        let chat = fb.db.me.chats[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "chatCell", for: indexPath) as! ChatsTableViewCell
        
        
        // Configure Cell
        if chat.messages.last == nil {
            return cell
        }
        
        let chatMessage = chat.messages.last! as Chat.Message
        cell.NameLabel.text = chat.profile.displayName
        if chatMessage.type ==  "image" {
            cell.IconResumeLabel.isHidden = false
            cell.IconResumeLabel.font = UIFont.fontAwesome(ofSize: 14)
            cell.IconResumeLabel.text = "\(String.fontAwesomeIcon(name: .camera)) "
            if chatMessage.subtitle == "" {
                cell.ResumeLabel.text = "Imagem"
            }else{
                cell.ResumeLabel.text = chatMessage.subtitle
            }
        }else{
            cell.IconResumeLabel.isHidden = true
            cell.ResumeLabel.text = chatMessage.content
        }
        cell.DateTimeLabel.text = Tools.box.IntToResumeDate(timestamp: chatMessage.date)
        
        if let imageData = fb.db.storageGetImageAvatar(chat.profile){
            cell.AvatarImageView.image = imageData
        }else{
            var iconAvatar = ""
            if(chat.profile.gender == "male"){
                iconAvatar = "IconeAvatarMOn"
            }
            else {
                iconAvatar = "IconeAvatarFOn"
            }
            cell.AvatarImageView.image = UIImage.init(named: iconAvatar)
        }
        cell.AvatarImageView.layer.borderWidth = 1.0
        cell.AvatarImageView.layer.borderColor = UIColor.lightGray.cgColor
        cell.AvatarImageView.layer.cornerRadius = cell.AvatarImageView.frame.size.width / 2
        cell.AvatarImageView.clipsToBounds = true
        if chat.unreadMessages > 0 {
            cell.badgeString = String(chat.unreadMessages)
        }else{
            cell.badgeString=""
        }
        if let imageData = fb.db.storageGetImageAvatar(chat.profile){
            cell.AvatarImageView.image = imageData
            cell.AvatarImageView.layer.borderWidth = 1.0
            cell.AvatarImageView.layer.borderColor = UIColor.lightGray.cgColor
            cell.AvatarImageView.layer.cornerRadius = cell.AvatarImageView.frame.size.width / 2
            cell.AvatarImageView.clipsToBounds = true
        }else{
            let imageSize = CGSize(width: cell.AvatarImageView.frame.width, height: cell.AvatarImageView.frame.width)
            cell.AvatarImageView.image = UIImage.fontAwesomeIcon(name: .user, textColor: .gray, size: imageSize)
        }
        
        return cell

    }



    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
        if let selectedIndex = self.tableView.indexPathForSelectedRow {
            if segue.identifier == "ChatDetailSegue"{
                if let destinationVC = segue.destination as? ChatVC{
                    destinationVC.chat = fb.db.me.chats[selectedIndex.row] as Chat
                    destinationVC.contactId = fb.db.me.chats[selectedIndex.row].contactId
                }
            }
        }
        
    }
}

