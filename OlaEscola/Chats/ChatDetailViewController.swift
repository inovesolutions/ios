import UIKit
import Photos
import Firebase
import CoreLocation

class ChatVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,  UINavigationControllerDelegate, UIImagePickerControllerDelegate, CLLocationManagerDelegate, ChatsProtocol, StorageChatDownloadProtocol {
    
    var initialScroll = true
    //MARK: Properties
    @IBOutlet var inputBar: UIView!
    @IBOutlet weak var tableViewChat: UITableView!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    override var inputAccessoryView: UIView? {
        get {
            self.inputBar.frame.size.height = self.barHeight
            self.inputBar.clipsToBounds = true
            return self.inputBar
        }
    }
    override var canBecomeFirstResponder: Bool{
        return true
    }
    
    
    @IBOutlet weak var extraIconMessage: UIButton!
    @IBOutlet weak var extraIconGallery: UIButton!
    @IBOutlet weak var extraIconCamera: UIButton!
    @IBOutlet weak var extraIconLocation: UIButton!
    @IBOutlet weak var extraIconMore: UIButton!
    @IBOutlet weak var extraIconSend: UIButton!
    @IBOutlet weak var extraBackgorund: UIVisualEffectView!

    // Chat Protocol
    func onAdded(_ chatId: String) {
        if self.contactId == chatId {
            self.chat = fb.db.me.chats.first(where: { (c) -> Bool in
                return c.contactId == chatId
            })!
            self.fetchData()
        }
    }
    func onChanged(_ chatId: String) {
        self.chat = fb.db.me.chats.first(where: { (c) -> Bool in
            return c.contactId == chatId
        })!
        if self.contactId == chatId {
            self.fetchData()
        }
    }
    func onRemoved(_ chatId: String) {
        if self.contactId == chatId {
            self.fetchData()
        }
    }
    
    
    // Chat Download Protocol
    
    func onResume(message: Chat.Message) {
        
    }
    
    func onFail(message: Chat.Message, error: Error) {
        if fb.db.chatLocalFileExists(message, self.contactId) { self.tableViewChat.reloadData() }
    }
    
    func onPause(message: Chat.Message) {
    
    }
    
    func onSuccess(message: Chat.Message) {
        if fb.db.chatLocalFileExists(message, self.contactId) { self.tableViewChat.reloadData() }
    }
    
    func onProgress(message: Chat.Message, completed: Float) {
        
    }
    
    
    let locationManager = CLLocationManager()
    let imagePicker = UIImagePickerController()
    let barHeight: CGFloat = 50
    var canSendLocation = true
    
    
    
    // OláEscola
    var chat = Chat()
    var contactId : String = ""
    
    //MARK: Methods
    func customization() {
        self.imagePicker.delegate = self
        self.inputTextField.delegate = self
        self.tableViewChat.estimatedRowHeight = self.barHeight
        self.tableViewChat.rowHeight = UITableViewAutomaticDimension
        self.tableViewChat.contentInset.bottom = self.barHeight
        self.tableViewChat.scrollIndicatorInsets.bottom = self.barHeight
        self.navigationItem.title = chat.profile.displayName
        self.navigationItem.setHidesBackButton(true, animated: false)
        let icon = UIImage.init(named: "back")?.withRenderingMode(.alwaysOriginal)
        let backButton = UIBarButtonItem.init(image: icon!, style: .plain, target: self, action: #selector(self.dismissSelf))
        self.navigationItem.leftBarButtonItem = backButton
        self.locationManager.delegate = self
        
        self.extraIconMore.setBackgroundImage(UIImage.fontAwesomeIcon(name: .paperclip, textColor: UIColor.white, size: CGSize(width: 32, height: 25)) , for: UIControlState.normal)
        self.extraIconMore.isEnabled = false
        self.extraIconSend.setBackgroundImage(UIImage.fontAwesomeIcon(name: .send, textColor: UIColor.white, size: CGSize(width: 32, height: 25)) , for: UIControlState.normal)
        self.extraIconMessage.setBackgroundImage(UIImage.fontAwesomeIcon(name: .commenting, textColor: UIColor.white, size: CGSize(width: 32, height: 25)) , for: UIControlState.normal)
        self.extraIconGallery.setBackgroundImage(UIImage.fontAwesomeIcon(name: .pictureO, textColor: UIColor.white, size: CGSize(width: 32, height: 25)) , for: UIControlState.normal)
        self.extraIconCamera.setBackgroundImage(UIImage.fontAwesomeIcon(name: .camera, textColor: UIColor.white, size: CGSize(width: 32, height: 25)) , for: UIControlState.normal)
        self.extraIconLocation.setBackgroundImage(UIImage.fontAwesomeIcon(name: .locationArrow, textColor: UIColor.white, size: CGSize(width: 32, height: 25)) , for: UIControlState.normal)
        self.extraIconLocation.isHidden = true

        self.extraBackgorund.backgroundColor = UIColor.rbg(r: 220, g: 248, b: 198)
        self.extraBackgorund.tintColor = UIColor.white
        let imageViewBackground = UIImageView(image: UIImage(named: "wallpaper"))
        imageViewBackground.contentMode = .scaleAspectFill
        imageViewBackground.alpha = 0.3
        self.tableViewChat.backgroundView = imageViewBackground
        
    }
    
    // Mostrar mensagens inicialmente
    func fetchData() {
        //self.chat.messages.sort{ $0.date < $1.date }
        self.tableViewChat.reloadData()
        let indexPaths = self.tableViewChat.indexPathsForVisibleRows
        if ( (initialScroll && self.chat.messages.count > 0)
            || (self.chat.messages.count > 0 && (indexPaths?.last?.row)! == self.chat.messages.count - 1) ){
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                self.tableViewChat.scrollToRow(at: IndexPath.init(row: self.chat.messages.count - 1, section: 0), at: .bottom, animated: false)
                self.initialScroll = false
            }
        }
        
        if(self.chat.writing){
            self.navigationItem.setTitle(title: self.chat.profile.displayName, subtitle: "Está escrevendo...")
        } else if(fb.db.presence[self.chat.contactId] == true){
            self.navigationItem.setTitle(title: self.chat.profile.displayName, subtitle: "Online")
        } else {
            self.navigationItem.setTitle(title: self.chat.profile.displayName, subtitle: "")
        }
    }
    
    // Esconder a viewController atual
    @objc func dismissSelf() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    func composeMessage(type: MessageType, content: Any)  {
        
        if type == .text {
            
        }else if type == .photo {
            
        }
 
        /*
        let message = ChatMessage.init(type: type, content: content, owner: .sender, timestamp: Int(Date().timeIntervalSince1970), isRead: false)
        ChatMessage.send(message: message, toID: Auth.auth().currentUser!.uid, completion: {(_) in
        })
         */
    }
    
    func checkLocationPermission() -> Bool {
        var state = false
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            state = true
        case .authorizedAlways:
            state = true
        default: break
        }
        return state
    }
    
    func animateExtraButtons(toHide: Bool)  {
        switch toHide {
        case true:
            self.bottomConstraint.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.inputBar.layoutIfNeeded()
            }
        default:
            self.bottomConstraint.constant = -50
            UIView.animate(withDuration: 0.3) {
                self.inputBar.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func showMessage(_ sender: Any) {
        self.animateExtraButtons(toHide: true)
    }
    
    @IBAction func selectGallery(_ sender: Any) {
        self.animateExtraButtons(toHide: true)
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == .authorized || status == .notDetermined) {
            self.imagePicker.sourceType = .savedPhotosAlbum;
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func selectCamera(_ sender: Any) {
        self.animateExtraButtons(toHide: true)
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if (status == .authorized || status == .notDetermined) {
            
            if !UIImagePickerController.isSourceTypeAvailable(.camera){
                
                let alertController = UIAlertController.init(title: nil, message: "Device has no camera.", preferredStyle: .alert)
                
                let okAction = UIAlertAction.init(title: "Alright", style: .default, handler: {(alert: UIAlertAction!) in
                })
                
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                
            }
            else{
                //other action
            }
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    

    
    func showCameraPicker() {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.modalPresentationStyle = UIModalPresentationStyle.currentContext
        picker.allowsEditing = false
        picker.sourceType = UIImagePickerControllerSourceType.camera
        present(picker, animated: true, completion: nil)
    }
    
    
    
    
    @IBAction func selectLocation(_ sender: Any) {
        self.canSendLocation = true
        self.animateExtraButtons(toHide: true)
        if self.checkLocationPermission() {
            self.locationManager.startUpdatingLocation()
        } else {
            self.locationManager.requestWhenInUseAuthorization()
        }
    }
    
    @IBAction func showOptions(_ sender: Any) {
        self.animateExtraButtons(toHide: false)
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        if let text = self.inputTextField.text {
            if text.count > 0 {
                fb.db.sendChatMessage(contactId: self.contactId, type: .text, content: self.inputTextField.text!)
                self.inputTextField.text = ""
            }
        }
    }
    
    //MARK: NotificationCenter handlers
    @objc func showKeyboard(notification: Notification) {
        if let frame = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let height = frame.cgRectValue.height
            self.tableViewChat.contentInset.bottom = height
            self.tableViewChat.scrollIndicatorInsets.bottom = height
            if chat.messages.count > 0 {
                self.tableViewChat.scrollToRow(at: IndexPath.init(row: chat.messages.count - 1, section: 0), at: .bottom, animated: true)
            }
        }
    }
    
    //MARK: Delegates
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chat.messages.count
    }
    
     func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableViewChat.isDragging {
            cell.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.3, animations: {
                cell.transform = CGAffineTransform.identity
            })
        }
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var returnCell : UITableViewCell = UITableViewCell()
        
        let item = self.chat.messages[indexPath.row]
        let me = fb.db.getUserId()
        
        if  item.senderId == me {
        
            if item.type == "text" {
                let cell = tableViewChat.dequeueReusableCell(withIdentifier: "Receiver", for: indexPath) as! ReceiverCell
                cell.clearCellData()
            
                let timeString = Tools.box.IntToDateOrTime(timestamp: item.date)
                cell.timeStatus.text = timeString
            
                if item.received {
                    cell.messageStatus.image = #imageLiteral(resourceName: "doubleCheckGray")
                    if item.read {
                        cell.messageStatus.image = #imageLiteral(resourceName: "doubleCheckBlue")
                    }
                }else{
                       cell.messageStatus.image = #imageLiteral(resourceName: "checkGray")
                }
            
                cell.message.text = item.content
                returnCell = cell

            } else if item.type == "image" {
                let cell = tableViewChat.dequeueReusableCell(withIdentifier: "ReceiverImage", for: indexPath) as! ReceiverCellImage
                cell.clearCellData()
                
                let timeString = Tools.box.IntToDateOrTime(timestamp: item.date)
                cell.timeStatus.text = timeString
                
                if item.received {
                    cell.messageStatus.image = #imageLiteral(resourceName: "doubleCheckGray")
                    if item.read {
                        cell.messageStatus.image = #imageLiteral(resourceName: "doubleCheckBlue")
                    }
                }else{
                    cell.messageStatus.image = #imageLiteral(resourceName: "checkGray")
                }
                
                cell.descriptionText.text = item.content
                
                returnCell = cell
            }

            
        }else{
            
            // A mensagem que eu recebi
            if item.type == "text" {
                let cell = tableViewChat.dequeueReusableCell(withIdentifier: "Sender", for: indexPath) as! SenderCell
                cell.clearCellData()
                
                // let timeString = Tools.box.NSDateToResumeDate(date: NSDate(timeIntervalSince1970: TimeInterval(item.date)))
                
                cell.message.text = item.content
                
                if self.chat.messages[indexPath.row].read == false {
                    self.chat.messages[indexPath.row].read = true
                    fb.db.checkChatMessageRead(contactId: chat.contactId, message: chat.messages[indexPath.row])
                }

                returnCell = cell
                
            } else if item.type == "image" {
                
                let cell = tableViewChat.dequeueReusableCell(withIdentifier: "SenderImage", for: indexPath) as! SenderCellImage
                cell.clearCellData()
                
                // Verifica se existe o arquivo local
                if fb.db.chatLocalFileExists(item, self.contactId) {
                    let localfileUrl = fb.db.chatLocalFile(item, self.contactId)
                    let imageData = NSData(contentsOf: localfileUrl!)
                    cell.messageImage.image = UIImage(data: imageData! as Data)
                    cell.downloadButton.isHidden = true
                }else{
                    cell.messageImage.image = nil
                    cell.messageImage.backgroundColor = UIColor.lightGray
                    cell.downloadButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 40)
                    cell.downloadButton.setTitle(String.fontAwesomeIcon(name: .camera), for: .normal)
                    cell.downloadButton.tag = indexPath.row
                    cell.downloadButton.isHidden = false
                    cell.message = item
                    cell.contactId = self.contactId
                    cell.downloadButton.target(forAction: Selector(("downloadButtonTap")), withSender: self)
                    
                }
                
                let timeString = Tools.box.NSDateToResumeDate(date: NSDate(timeIntervalSince1970: TimeInterval(item.date)))
                cell.timeStatus.text = timeString
                
                /*
                if item.received {
                    cell.messageStatus.image = #imageLiteral(resourceName: "doubleCheckGray")
                    if item.read {
                        cell.messageStatus.image = #imageLiteral(resourceName: "doubleCheckBlue")
                    }
                }else{
                    cell.messageStatus.image = #imageLiteral(resourceName: "checkGray")
                }
                */
                
                cell.descriptionText.text = item.subtitle
                
                if self.chat.messages[indexPath.row].read == false && self.chat.messages[indexPath.row].uploaded {
                    self.chat.messages[indexPath.row].read = true
                    fb.db.checkChatMessageRead(contactId: chat.contactId, message: chat.messages[indexPath.row])
                }
                returnCell = cell
            }
            
        }
        
        return returnCell
    }
    /*
    //QUANDO CLICAR NA MENSAGEM
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentMessage = self.chat.messages[indexPath.row]
        switch currentMessage.type {
        case "image":
            if fb.db.chatLocalFileExists(currentMessage, self.contactId) {
                let localfileUrl = fb.db.chatLocalFile(currentMessage, self.contactId)
                let imageData = NSData(contentsOf: localfileUrl!)
                if let photo = UIImage(data: imageData! as Data) {
                    let info = ["viewType" : ShowExtraView.preview, "pic": photo] as [String : Any]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showExtraView"), object: nil, userInfo: info)
                    self.inputAccessoryView?.isHidden = true
                    print("imagem clicada")
                }
            }
        case "location":
            let coordinates = (currentMessage.content).components(separatedBy: ":")
            let location = CLLocationCoordinate2D.init(latitude: CLLocationDegrees(coordinates[0])!, longitude: CLLocationDegrees(coordinates[1])!)
            let info = ["viewType" : ShowExtraView.map, "location": location] as [String : Any]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showExtraView"), object: nil, userInfo: info)
            self.inputAccessoryView?.isHidden = true
        default: break
        }
        
        //self.inputTextField.resignFirstResponder()
    }
    */
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.composeMessage(type: .photo, content: pickedImage)
        } else {
            let pickedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
            self.composeMessage(type: .photo, content: pickedImage)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationManager.stopUpdatingLocation()
        if locations.last != nil {
            if self.canSendLocation {
                //let coordinate = String(lastLocation.coordinate.latitude) + ":" + String(lastLocation.coordinate.longitude)
                //let message = ChatMessage.init(type: .location, content: coordinate, owner: .sender, timestamp: Int(Date().timeIntervalSince1970), isRead: false)
                //ChatMessage.send(message: message, toID: self.currentUser!.id, completion: {(_) in
                //})
                self.canSendLocation = false
            }
        }
    }
    
    //MARK: ViewController lifecycle
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.inputBar.backgroundColor = UIColor.clear
        self.view.layoutIfNeeded()
        NotificationCenter.default.addObserver(self, selector: #selector(ChatVC.showKeyboard(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        fb.db.chatProtocol = nil
        fb.db.chatDownloadProtocol = nil
        //ChatMessage.markMessagesRead(forUserID: Auth.auth().currentUser!.uid)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewChat.delegate = self
        tableViewChat.dataSource = self
        fb.db.chatProtocol = self
        fb.db.chatDownloadProtocol = self
        self.customization()
        self.fetchData()
        inputTextField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        if (self.chat.lastChange == 0) { return }
        fb.db.setWriting(chatId: self.chat.contactId)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let selectedIndex = self.tableViewChat.indexPathForSelectedRow{
            if segue.identifier == "ChatDetailImageSegue"{
                if let destinationVC = segue.destination as? ChatDetailImageViewController{
                    destinationVC.message = self.chat.messages[selectedIndex.row]
                    destinationVC.contactId = self.contactId
                }
            }
        }
    }
    
}





class SenderCell: UITableViewCell {
    
    @IBOutlet weak var profilePic: RoundedImageView!
    @IBOutlet weak var message: UITextView!
    @IBOutlet weak var messageBackground: UIImageView!
    
    func clearCellData()  {
        self.message.text = nil
        self.message.isHidden = false
        self.messageBackground.image = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.message.textContainerInset = UIEdgeInsetsMake(5, 5, 5, 5)
        self.messageBackground.layer.cornerRadius = 15
        self.messageBackground.clipsToBounds = true
        self.messageBackground.backgroundColor = UIColor.white
        self.message.textColor = UIColor.black
        self.backgroundColor = .clear
    }
}

class ReceiverCell: UITableViewCell {
    
    @IBOutlet weak var message: UITextView!
    @IBOutlet weak var messageBackground: UIImageView!
    @IBOutlet weak var timeStatus: UILabel!
    @IBOutlet weak var messageStatus: UIImageView!
    
    func clearCellData()  {
        self.message.text = nil
        self.message.isHidden = false
        self.messageBackground.image = nil
        self.timeStatus.text = nil
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.message.textContainerInset = UIEdgeInsetsMake(5, 5, 5, 5)
        self.messageBackground.layer.cornerRadius = 15
        self.messageBackground.clipsToBounds = true
        self.messageBackground.backgroundColor = UIColor.init(cgColor:  GlobalVariables.colorBubbleMe.cgColor )
        self.message.textColor = UIColor.black
        self.backgroundColor = .clear
    }
}


class SenderCellImage: UITableViewCell {
    
    @IBOutlet weak var messageImage: UIImageView!
    @IBOutlet weak var messageStatus: UIImageView!
    @IBOutlet weak var descriptionBackground: UIImageView!
    @IBOutlet weak var timeStatus: UILabel!
    @IBOutlet weak var descriptionText: UILabel!
    @IBOutlet weak var progresssStatus: UIActivityIndicatorView!
    @IBOutlet weak var downloadButton: UIButton!
    
    public var message : Chat.Message = Chat.Message()
    public var contactId : String = ""
    
    @IBAction func imageTapped(sender: ChatVC) {
        print("Image Tapped.")
    }
    
    @IBAction func downloadButtonTap(_ sender: ChatVC) {
        // inicia o download
        let ImageReference = Storage.storage().reference().child("users").child(fb.db.getUserId()).child("chats").child(self.message.senderId).child(self.message.messageId).child(message.content)
        if let localFileUrl = fb.db.chatLocalFile(message, contactId) {
            let downloadTask = ImageReference.write(toFile: localFileUrl)

            downloadTask.observe(.resume) { (snapshot) in
                // mostra a animacao
                self.downloadButton.isHidden = true
                self.progresssStatus.isHidden = false
                self.progresssStatus.startAnimating()
            }

            downloadTask.observe(.pause) { (snapshot) in
                // mostra a animacao
                self.downloadButton.isHidden = false
                self.progresssStatus.isHidden = true
                self.progresssStatus.stopAnimating()
            }

            downloadTask.observe(.failure) { (snapshot) in
                if snapshot.error != nil {
                    //sender.alert(message: "Houve um erro ao tentar baixar o arquivo \(self.message.content) do contato \(self.contactId)")
                    print("Houve um erro ao tentar baixar o arquivo \(self.message.content) do contato \(self.contactId)")
                    print(snapshot.error!.localizedDescription)
                    print(snapshot.error.debugDescription)
                    self.downloadButton.isHidden = false
                    self.progresssStatus.isHidden = true
                    self.progresssStatus.stopAnimating()
                    return
                }
            }

            downloadTask.observe(.success) { (snapshot) in
                print("Download completed (IMAGE CHAT):  \(self.message.content)")
                if let imageLoaded = fb.db.chatGetLocalFile(self.message, self.contactId) {
                    self.messageImage.image = imageLoaded
                    self.downloadButton.isHidden = true
                    self.progresssStatus.isHidden = true
                    self.progresssStatus.stopAnimating()
                    fb.db.checkChatMessageRead(contactId: self.contactId, message: self.message)
                }else{
                    //sender.alert(message: "Não foi possível carregar a imagem a partir do seu dispositivo.")
                    print("Houve um erro ao tentar recuperar a imagem baixaa: \(self.message.content) do contato \(self.contactId)")
                }
                
            }

        }
    }
    
    func clearCellData()  {
        self.timeStatus.text = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.messageImage.layer.cornerRadius = 15
        self.messageImage.clipsToBounds = true
        self.descriptionBackground.clipsToBounds = true
        self.descriptionBackground.layer.cornerRadius = 15
        if #available(iOS 11.0, *) {
            self.descriptionBackground.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        }
        self.backgroundColor = .clear
    }
}

class ReceiverCellImage: UITableViewCell {
    
    @IBOutlet weak var messageImage: UIImageView!
    @IBOutlet weak var descriptionBackground: UIImageView!
    @IBOutlet weak var messageStatus: UIImageView!
    @IBOutlet weak var timeStatus: UILabel!
    @IBOutlet weak var descriptionText: UILabel!
    
    func clearCellData()  {
        self.timeStatus.text = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        
        self.messageImage.layer.cornerRadius = 15
        self.messageImage.clipsToBounds = true
        self.descriptionBackground.clipsToBounds = true
        self.descriptionBackground.layer.cornerRadius = 15
        if #available(iOS 11.0, *) {
            self.descriptionBackground.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        }
        
        self.backgroundColor = .clear
    }
}







