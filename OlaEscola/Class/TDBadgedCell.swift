//
//  TDBadgedCell.swift
//  TDBadgedCell
//
//  Created by Tim Davies on 07/09/2016.
//  Copyright © 2016 Tim Davies. All rights reserved.
//

import UIKit


open class TDBadgedCell: UITableViewCell {

    /// Badge value
    public var badgeString : String = "" {
        didSet {
            if(badgeString == "") {
                badgeView.removeFromSuperview()
                layoutSubviews()
            } else {
                contentView.addSubview(badgeView)
                drawBadge()
            }
        }
    }
    
    /// Badge background color for normal states
    public var badgeColor : UIColor = UIColor.red  //(red: 0, green: 0.478, blue: 1, alpha: 1.0)
    /// Badge background color for highlighted states
    public var badgeColorHighlighted : UIColor = .darkGray

    /// Badge font size
    public var badgeFontSize : Float = 11.0
    /// Badge text color
    public var badgeTextColor: UIColor? = UIColor.white
    /// Corner radius of the badge. Set to 0 for square corners.
    public var badgeRadius : Float = 10
    /// The Badges offset from the right hand side of the Table View Cell
    public var badgeOffset = CGPoint(x:10, y:0)
    
    /// The Image view that the badge will be rendered into
    internal let badgeView = UIImageView()
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        // Layout our badge's position
        var offsetX = badgeOffset.x
        if(isEditing == false && accessoryType != .none || (accessoryView) != nil) {
            offsetX = 0 // Accessory types are a pain to get sizing for?
        }
        
        badgeView.frame.origin.x = floor(contentView.frame.width - badgeView.frame.width - offsetX)
        //badgeView.frame.origin.y = floor((frame.height / 2) - (badgeView.frame.height / 2))
        badgeView.frame.origin.y = floor((frame.height / 2))
        
        // Now lets update the width of the cells text labels to take the badge into account
        textLabel?.textAlignment = NSTextAlignment.center
        //textLabel?.frame.size.width -= badgeView.frame.width + (offsetX * 2)
        if((detailTextLabel) != nil) {
            detailTextLabel?.frame.size.width -= badgeView.frame.width + (offsetX * 2)
        }
    }
    
    // When the badge
    override open func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        drawBadge()
    }
    
    override open func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        drawBadge()
    }
    
    /// Generate the badge image
    internal func drawBadge() {
        // Calculate the size of our string
        let textSize : CGSize = NSString(string: badgeString).size(withAttributes:[NSAttributedStringKey.font:UIFont.boldSystemFont(ofSize:CGFloat(badgeFontSize))])
        
        // Create a frame with padding for our badge
        let height = textSize.height + 5
        var width = textSize.width + 8
        if(width < height) {
            width = height
        }
        let badgeFrame : CGRect = CGRect(x:0, y:0, width:width, height:height)
        
        let badge = CALayer()
        badge.frame = badgeFrame
        
        if(isHighlighted || isSelected) {
            badge.backgroundColor = badgeColorHighlighted.cgColor
        } else {
            badge.backgroundColor = badgeColor.cgColor
        }

        badge.cornerRadius = (CGFloat(badgeRadius) < (badge.frame.size.height / 2)) ? CGFloat(badgeRadius) : CGFloat(badge.frame.size.height / 2)
        
        // Draw badge into graphics context
        UIGraphicsBeginImageContextWithOptions(badge.frame.size, false, UIScreen.main.scale)
        let ctx = UIGraphicsGetCurrentContext()!
        ctx.saveGState()
        badge.render(in:ctx)
        ctx.saveGState()
        
        // Draw string into graphics context
        if(badgeTextColor == nil) {
            ctx.setBlendMode(CGBlendMode.clear)
        }
        
        NSString(string: badgeString).draw(in:CGRect(x:((badge.frame.size.width-textSize.width)/2), y:2.5, width:textSize.width, height:textSize.height), withAttributes: [
            NSAttributedStringKey.font:UIFont.boldSystemFont(ofSize:CGFloat(badgeFontSize)),
            NSAttributedStringKey.foregroundColor: badgeTextColor ?? UIColor.clear
            ])

        let badgeImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()


        badgeView.frame = CGRect(x:0, y:0, width:badgeImage.size.width, height:badgeImage.size.height)
        badgeView.image = badgeImage
        
        layoutSubviews()
    }
}





class Cell: UITableViewCell {

    override func draw(_ rect: CGRect) {
        
        var bubbleSpace = CGRect(x: 20.0, y: self.bounds.origin.y, width: self.bounds.width - 20, height: self.bounds.height)

        let bubblePath1 = UIBezierPath(roundedRect: bubbleSpace, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 20.0, height: 20.0))
        
        let bubblePath = UIBezierPath(roundedRect: bubbleSpace, cornerRadius: 20.0)
        
        UIColor.green.setStroke()
        UIColor.green.setFill()
        bubblePath.stroke()
        bubblePath.fill()
        
        var triangleSpace = CGRect(x: 0.0, y: self.bounds.height - 20, width: 20.0, height: 20.0)
        var trianglePath = UIBezierPath()
        var startPoint = CGPoint(x: 20.0, y: self.bounds.height - 40)
        var tipPoint = CGPoint(x: 0.0, y: self.bounds.height - 30)
        var endPoint = CGPoint(x: 20.0, y: self.bounds.height - 20)
        trianglePath.move(to: startPoint) //moveToPoint(startPoint)
        trianglePath.addLine(to: tipPoint) //.addLineToPoint(tipPoint)
        trianglePath.addLine(to: endPoint) //addLineToPoint(endPoint)
        trianglePath.close()  //.closePath()
        UIColor.green.setStroke()
        UIColor.green.setFill()
        trianglePath.stroke()
        trianglePath.fill()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
            var backgroundImage = UIImageView(image: UIImage(named: "foto"))
            backgroundImage.contentMode = UIViewContentMode.scaleAspectFit
            self.backgroundView = backgroundImage
    }
}
