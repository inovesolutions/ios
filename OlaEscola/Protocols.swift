//
//  Protocols.swift
//  Olá Escola
//
//  Created by Rafael Marinheiro on 28/08/17.
//  Copyright © 2017 Bmtech Soluções Tecnológicas. All rights reserved.
//

import Foundation

protocol UsersProtocol {
    func onChanged()
    func onAdded()
    func onRemoved()
}

protocol ProfileProtocol {
    func onChangedProfile()
    func onAddedProfile()
    func onRemovedProfile()
}

protocol ChatsProtocol {
    func onChanged(_ chatId : String)
    func onAdded(_ chatId : String)
    func onRemoved(_ chatId : String)
}

protocol RelationsProtocol {
    func onChanged()
    func onAdded()
    func onRemoved()
}

protocol MessagesProtocol {
    func onChanged()
    func onAdded()
    func onRemoved()
}

protocol ContactsProtocol {
    func onChanged()
    func onAdded()
    func onRemoved()
}

protocol TagsProtocol {
    func onChanged()
    func onAdded()
    func onRemoved()
}

protocol ModulesProtocol {
    func onChanged()
    func onAdded()
    func onRemoved()
}

protocol StorageImageDownloadProtocol {
    func onSuccess()
    func onFail(error: Error)
    func onPause()
    func onResume()
    func onProgress(completed: Float)
}


protocol StorageChatDownloadProtocol {
    func onSuccess(message: Chat.Message)
    func onFail(message: Chat.Message, error: Error)
    func onPause(message: Chat.Message)
    func onResume(message: Chat.Message)
    func onProgress(message: Chat.Message, completed: Float)
}

protocol AuthProtocol {
    func authRequire()
}

// Atualizar o avatar
protocol StorageAvatarUpdateProtocol {
    func onUpdateAvatar()
}

protocol BadgesUpdateProtocol{
    func messagesUpdated()
    func chatsUpdated(chatId: String)
}
