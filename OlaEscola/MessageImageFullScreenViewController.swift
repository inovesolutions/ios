//
//  MessageImageFullScreenViewController.swift
//  OlaEscola
//
//  Created by Developer on 22/01/2018.
//  Copyright © 2018 Bmtech Soluções Tecnológicas. All rights reserved.
//


import UIKit

class MessageImageFullScreenViewController: UIViewController {
    
    @IBOutlet weak var imageScrollView: ImageScrollView!
    @IBOutlet weak var SubtitleText: UILabel!
    @IBAction func BackButtonTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    var message : Message = Message()
    var contactId : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = ""
        var localFileUrl = FileManager.default.urls(for: .documentDirectory, in:.userDomainMask).first!
        localFileUrl.appendPathComponent("images", isDirectory: true)
        localFileUrl.appendPathComponent(message.content.body.content)
        if (FileManager.default.fileExists(atPath: localFileUrl.path)) {
            let imageData = NSData(contentsOf: localFileUrl)
            if let photo = UIImage(data: imageData! as Data) {
                self.imageScrollView.display(image: photo)
            }
        }
        self.SubtitleText.attributedText = self.message.content.body.imageDescription?.html2AttributedString
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
