//
//  model.swift
//  Olá Escola
//
//  Created by Rafael Marinheiro on 24/08/17.
//  Copyright © 2017 Bmtech Soluções Tecnológicas. All rights reserved.
//

import UIKit
import Firebase

class User : NSObject {
    
    var userId : String = ""
    var profile = Profile()
    var relations : Array<Relation> = []
    var messages : Array<Message> = []
    var chats : Array<Chat> = []
    var tags : Array<String> = []
    var contacts : Array<Contact> = []

    
    func fill(userId: String, _ dict: [String : Any]) -> User {
    
        relations.removeAll()
        messages.removeAll()
        chats.removeAll()
        tags.removeAll()
        contacts.removeAll()
    
        self.profile.fill(userId , dict["profile"] as! [String:Any])
        
        for (key, value) in dict["relations"] as? [String:Any] ?? [:] {
            let relation = Relation()
            relation.relationId = key as String
            relation.fill( key: key, dict: (value as? [String: Any] )!)
            self.relations.append(relation)
        }
        
        for (key, value) in dict["messages"] as? [String:Any] ?? [:]{
            let message = Message()
            message.messageId = key as String
            message.fill(key, (value as? [String: Any])!)
            if message.deleted == false {
                self.messages.append(message)
            }
        }
        
        for (key, _) in dict["chats"] as? [String:Any] ?? [:]{
            let chat = Chat()
            chat.contactId = key as String
            //chat.fill(key as String, (value as? [String: Any])!)
            self.chats.append(chat)
        }
        
        for (key , _) in dict["tags"] as? [String:Any] ?? [:]{
            self.tags.append(key)
        }
        
        
        for (key, value) in dict["contacts"] as? [String:Any] ?? [:] {
            let contact = Contact()
            contact.profileId = key as String
            contact.fill( key, (value as? [String: Any])!)
            self.contacts.append(contact)
        }
        
        return self
    }
    
   /*
    func fillChats(_ chatsSnapshotValue: [String:Any]  ){
        self.chats.removeAll()
        for (key, value) in chatsSnapshotValue {
            let chat = Chat()
            chat.contactId = key as String
            chat.fill(key as String, (value as? [String: Any])!)
            self.chats.append(chat)
        }
    }
     */
    
    func getContactById(_ id: String) -> Contact?
    {
        for item in contacts{
            if item.profileId == id {
                return item
            }
        }
        return nil
    }

    func getProfileById(_ id: String) -> Profile?
    {
        for item in contacts{
            if item.profileId == id {
                return item as Profile
            }
        }
        return nil
    }

    func getChatById(_ id: String) -> Chat?
    {
        for item in chats{
            if item.contactId == id {
                return item
            }
        }
        return nil
    }
    
}

class Profile : NSObject {
    
    var displayName : String = ""
    var displaySubtitle : String = ""
    var avatar : String = ""
    var email : String = ""
    var mobilePhone : String = ""
    var gender : String = ""
    var type : String = ""
    var profileId : String = ""
    
    func fill(_ key: String,_ dict: [String : Any]) {
        self.displayName = dict["displayName"] as? String ?? ""
        self.displaySubtitle = dict["displaySubtitle"] as? String ?? ""
        self.avatar = dict["avatar"] as? String ?? ""
        self.email = dict["email"] as? String ?? ""
        self.mobilePhone = dict["mobilePhone"] as? String ?? ""
        self.gender = dict["gender"] as? String ?? ""
        self.type = dict["type"] as? String ?? ""
        self.profileId = key as String
    }
    
    func toDict() -> [String:Any] {
        var dict : [String: Any] = [:]
        dict["displayName"] = self.displayName
        dict["displaySubtitle"] = self.displaySubtitle
        dict["avatar"] = self.avatar
        dict["email"] = self.email
        dict["mobilePhone"] = self.mobilePhone
        dict["gender"] = self.gender
        dict["type"] = self.type
        dict["prodileId"] = self.profileId
        return dict
    }
    
}


class Relation : NSObject {
    
    var relationId : String = ""
    var types : Array<String> = []
    var profile = Profile()
    
    func fill(key: String, dict: [String:Any]){
        self.relationId = key as String
        self.profile.fill(key, dict["profile"] as? [String:Any] ?? [:])
        self.types = dict["types"] as? Array<String> ?? []
    }
    
}

class Message : NSObject{
    
    var messageId : String = ""
    let content = Content()
    var received : Bool? = nil
    var read : Bool? = nil
    var deleted : Bool? = nil
    
    
    class Content : NSObject{
        
        var type: String = ""
        let header = Header()
        let body = Body()
        
        func fill(_ dict: [String : Any]) {
            self.type = dict["type"] as? String ?? ""
            self.header.fill(dict["header"] as? [String : Any] ?? [:])
            self.body.fill(dict["body"] as? [String : Any] ?? [:])
            
        }
        
        class Header : NSObject{
            var date: String = ""
            var moduleId : String = ""
            let moduleVersion : Version = Version()
            var moduleIcon : String = ""
            let moduleIconColor : RGB = RGB()
            let moduleIconBackground : RGB = RGB()
            var senderId : String = ""
            var senderName : String = ""
            var senderAvatar : String = ""
            var relationTypes : Array<String> = []
            var relationUsers : Array<String> = []
            var relations : Array<Relation> = []
            
            func fill(_ dict: [String : Any]) {
                self.date = dict["date"] as! String
                self.moduleId = dict["moduleId"] as! String
                self.moduleVersion.fill(dict["moduleVersion"] as! [String : Any])
                self.moduleIcon = dict["moduleIcon"] as? String ?? "" as String
                self.moduleIconColor.fill(dict["moduleIconColor"] as! [String : Any])
                self.moduleIconBackground.fill(dict["moduleIconBackground"] as! [String : Any])
                self.senderId = dict["senderId"] as! String
                self.senderName = dict["senderName"] as! String
                self.senderAvatar = dict["senderAvatar"] as! String
                self.relationTypes = dict["relationTypes"] as? Array ?? []
                self.relationUsers = dict["relationUsers"] as? Array ?? []
                for (key, value) in dict["relations"] as? [String:Any] ?? [:]{
                    let relation = Relation()
                    relation.relationId = key as String
                    relation.fill(key: key , dict: value as! [String: Any])
                    self.relations.append(relation)
                }
                
            }
        }
        
        class Body : NSObject {
            var title : String = ""
            var subTitle : String = ""
            var content : String = ""
            var imageDescription : String? = ""
            
            func fill(_ dict: [String : Any]) {
                self.title = dict["title"] as! String
                self.subTitle = dict["subtitle"] as! String
                self.content = dict["content"] as! String
                self.imageDescription = dict["imageDescription"] as? String
            }
        }
    }
    
    func fill(_ key: String, _ dict: [String : Any]) {
        
        self.messageId = key as String
        self.received = dict["received"] as? Bool ?? false
        self.read = dict["read"] as? Bool ?? false
        self.deleted = dict["deleted"] as? Bool ?? false
        self.content.fill(dict["content"] as? [String : Any] ?? [:])
    }
    
}


class Version : NSObject {
    
    var android : String?
    var ios : String?
    var web : String?
    
    func fill(_ dict: [String : Any]) {
        self.android = (dict["android"] as? String ?? "1.0.0")
        self.ios = (dict["ios"] as? String ?? "1.0.0")
        self.web = (dict["web"] as? String ?? "1.0.0")
        
    }
    
}

class Chat : NSObject {
    
    var contactId : String = ""
    var messages : Array<Message> = []
    var profile : Profile = Profile()
    var lastChange : Int64 = 0
    var unreadMessages  = 0
    var writing : Bool = false
    
    func fill(snap: DataSnapshot) {
        var messagesTemp : Array<Message> = []
        self.contactId = snap.key
        self.unreadMessages = 0
        self.lastChange = snap.childSnapshot(forPath: "lastChange").value as! Int64
        if(snap.childSnapshot(forPath: "writing").exists()){
            self.writing = snap.childSnapshot(forPath: "writing").value as! Bool
        }
        for item in snap.childSnapshot(forPath: "messages").children.allObjects {
            let messageSnapshot = item as! DataSnapshot
            let message = Message()
            message.messageId = messageSnapshot.key as String
            message.fill(message.messageId , messageSnapshot.value as! [String: Any])
            if !message.deleted {
                if message.type == "image" && message.uploaded == false {
                    continue
                }
                messagesTemp.append(message)
                if message.senderId == contactId {
                    if message.read == false {
                        unreadMessages = unreadMessages + 1
                    }
                    if message.received == false {
                        fb.db.checkChatMessageReceived(contactId: contactId, message: message)
                    }
                }
            }
        }
        profile.fill(snap.key, snap.childSnapshot(forPath: "profile").value as? [String:Any] ?? [:])
        self.messages = messagesTemp
    }

    
    internal class Message : NSObject{
        var messageId: String = ""
        var senderId : String = fb.db.getUserId()
        var content : String = ""
        var type : String = ""
        var date : Int64 = 0
        var deleted = false
        var sent = false
        var received = false
        var read = false
        var uploaded = false
        var subtitle : String = ""
        var version = Version()
        
        func fill(_ key: String, _ dict: [String : Any]) {
            if dict["senderId"] != nil {
            self.messageId = key as String
            self.senderId = dict["senderId"] as! String
            self.content = dict["content"] as! String
            self.type = dict["type"] as! String
            self.date = dict["date"] as! Int64
            self.deleted = dict["deleted"] as! Bool
            self.received = dict["received"] as! Bool
            self.read = dict["read"] as! Bool
            self.subtitle = dict["subtitle"] as? String ?? ""
            self.uploaded = dict["uploaded"] as? Bool ?? false
            self.version.fill(dict["version"] as? [String:Any] ?? [:])
            }
        }
        
        func toDict() ->[String:Any]{
            var dict : [String:Any] = [:]
            dict["senderId"] = self.senderId
            dict["content"] = self.content
            dict["type"] = self.type
            dict["date"] = self.date
            dict["deleted"] = self.deleted
            dict["received"] = self.received
            dict["read"] = self.read
            dict["uploaded"] = self.uploaded
            // RESOLVER PENDENCIA (VERSION)
            return dict
        }

    }
    
}


class Contact : Profile{
    
}

class RGB : NSObject {
    
    var R : CGFloat = 0.0
    var G : CGFloat = 0.0
    var B : CGFloat = 0.0
    
    func fill(_ dictRGB: [String : Any]) {
        self.R = (dictRGB["r"] as? CGFloat ?? 255) / 255
        self.G = (dictRGB["g"] as? CGFloat ?? 255) / 255
        self.B = (dictRGB["b"] as? CGFloat ?? 255) / 255
    }

    func set(red: NSInteger, green: NSInteger, blue: NSInteger) {
        self.R = CGFloat(red / 255)
        self.G = CGFloat(green / 255)
        self.B = CGFloat(blue / 255)
    }

    func getUIColor() -> UIColor {
        return UIColor (red: CGFloat(self.R), green: CGFloat(self.G ), blue: CGFloat(self.B ), alpha: 1.0)
    }
}


class Module : NSObject {
    
    var moduleId : String = ""
    var name : String = ""
    var icon : String = ""
    let version = Version()
    
    func fill(_ key: String, _ dict: [String : Any]) {
        self.name = dict["name"] as! String
        self.moduleId = key as String
        self.icon = dict["icon"] as! String
        self.version.fill(dict["version"] as! [String:Any])
    }
    
}




