//
//  SettingsTableViewController.swift
//  Olá Escola
//
//  Created by Rafael Marinheiro on 23/08/17.
//  Copyright © 2017 Bmtech Soluções Tecnológicas. All rights reserved.
//

import UIKit
import Firebase
import PhoneNumberKit


class SettingsProfileCell: UITableViewCell{
    // Section 1 - Profile
    @IBOutlet weak var profileAvatarImageView: UIImageView!
    @IBOutlet weak var profileFirstNameLabel: UILabel!
    @IBOutlet weak var profileLastNameLabel: UILabel!
    @IBOutlet weak var profileEmailLabel: UILabel!
    @IBOutlet weak var profilePhoneLabel: UILabel!
    
}

class SettingsRelationsCell: UITableViewCell{
    // Section 2 - Relations
    @IBOutlet weak var relationsAvatarImageView: UIImageView!
    @IBOutlet weak var relationsNameLabel: UILabel!
    @IBOutlet weak var relationsTypeLabel: UILabel!
}


class SettingsOptionsCell: UITableViewCell{
    
    // Section 3 - Options
    
    @IBOutlet weak var optionsTitleLabel: UILabel!
    @IBOutlet weak var optionsChoiceSwitch: UISwitch!
    @IBOutlet weak var optionsButton: UIButton!
    
    @IBAction func SwitchOption(_ sender: UISwitch) {
        
        if sender.accessibilityIdentifier  == "Habilita Som" {
            Tools.box.settings.setPlaySplash(sender.isOn)
        }

    }

    @IBAction func TapButton(_ sender: UIButton) {
        if sender.titleLabel?.text == "Remover" {
            if Auth.auth().currentUser != nil {
                fb.db.logout()
            }
        }else if sender.titleLabel?.text == "Entrar" {
            fb.db.authProtocol?.authRequire()
        }
    }

    
}

class SettingsTableViewController: UITableViewController, UsersProtocol, StorageImageDownloadProtocol, AuthProtocol, ProfileProtocol {

    // AuthProtocol
    func authRequire() { self.authenticationScreen() }

    // ProfileProtocol
    func onChangedProfile() {
        tableView.reloadData()
    }
    func onAddedProfile() {}
    func onRemovedProfile() {}
    
    // UsersProtocol
    func onAdded() {}
    func onChanged() {
        tableView.reloadData()
    }
    func onRemoved() {}
    
    //StorageImageDownloadProtocol
    func onPause() {}
    func onResume() {}
    func onSuccess() {
        tableView.reloadData()
    }
    func onFail(error: Error) {}
    func onProgress(completed: Float) {}
    

    
    var relations : Array<Relation> = []
    var sectionsNames : Array<String?> = ["Perfil","Relacionamentos","Opções"]
    
    var settingsOptionsItens : Array<String> = ["Habilita Som", "Conta"]
   
    override func viewDidLoad() {
        super.viewDidLoad()
            fb.db.authProtocol = self
            fb.db.userProtocol = self
            fb.db.storageImageDownloadProtocol = self
            fb.db.profileProtocol = self
            self.relations = fb.db.me.relations
    }
 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return sectionsNames.count
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.relations.count > 0 {
            return sectionsNames[section]
        }else{
            var tempSections = sectionsNames
            tempSections[1] = nil
            return tempSections[section]
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if Auth.auth().currentUser == nil && section != 2 { // Caso não tenha perfil
            return 0
        }

        if section == 0 { // Profile
            return 1
        }else if section == 1 { // Relations
            if self.relations.count > 0 {
                return self.relations.count
            }
        }else if section == 2 { // Config
            return settingsOptionsItens.count
        }
        return 0
    }
    

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0 {
            return 150.0; // Profile Cell
        }else if indexPath.section == 1 {
            return 66.0; // Relations Cell
        }else {
            return 50.0; // Others Cell, options inclusive
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Profile
        if indexPath.section == 0 {
            
            let profile = fb.db.me.profile
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "profileCell", for: indexPath) as! SettingsProfileCell
            
            if profile.displayName == "" {
                return cell
            }
            
            let fullName    = profile.displayName
            let fullNameArr = fullName.components(separatedBy: " ")
            
            let name    = fullNameArr[0]
            let surnameArr = fullName.components(separatedBy:  name+" ")
            let surname = surnameArr[1]
            
            var iconAvatar = ""
            if(profile.gender == "male"){
                iconAvatar = "IconeAvatarMOn"
            }
            else {
                iconAvatar = "IconeAvatarFOn"
            }
            
            cell.profileAvatarImageView.image = UIImage.init(named: iconAvatar)
            cell.profileAvatarImageView.layer.borderWidth = 2.0
            cell.profileAvatarImageView.layer.borderColor = UIColor.white.cgColor
            cell.profileAvatarImageView.layer.cornerRadius = cell.profileAvatarImageView.frame.size.width / 2
            cell.profileAvatarImageView.clipsToBounds = true
            
            fb.db.storageGetImageAvatar(profile){ (image) -> () in
                cell.profileAvatarImageView.image = image
            }

            cell.profileFirstNameLabel.text = name
            cell.profileLastNameLabel.text = surname
            cell.profileEmailLabel.text = profile.email
            
            let phoneNumberKit = PhoneNumberKit()
            do {
                let phoneNumber  = try phoneNumberKit.parse(profile.mobilePhone)
                cell.profilePhoneLabel.text = phoneNumberKit.format(phoneNumber, toType: .international)
            }
            catch {
                cell.profilePhoneLabel.text = profile.mobilePhone
            }
            
            
            return cell
        
        
        // Relations
        }else if indexPath.section == 1 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "relationsCell", for: indexPath) as! SettingsRelationsCell
            let relationTypes = fb.db.me.relations[indexPath.row].types
            let relationProfile = fb.db.me.relations[indexPath.row].profile
            
            if let imageData = fb.db.storageGetImageAvatar(relationProfile){
                cell.relationsAvatarImageView.image = imageData
            }else{
                var iconAvatar = ""
                if(relationProfile.gender == "male"){
                    iconAvatar = "IconeAvatarMOn"
                }
                else {
                    iconAvatar = "IconeAvatarFOn"
                }
                cell.relationsAvatarImageView.image = UIImage.init(named: iconAvatar)
            }
            
            cell.relationsAvatarImageView.layer.borderWidth = 2.0
            cell.relationsAvatarImageView.layer.borderColor = UIColor.white.cgColor
            cell.relationsAvatarImageView.layer.cornerRadius=cell.relationsAvatarImageView.frame.size.width / 2
            cell.relationsAvatarImageView.clipsToBounds = true
            
            cell.relationsNameLabel.text = relationProfile.displayName
            
            cell.relationsTypeLabel.text = ""
            for item in relationTypes {
                if(cell.relationsTypeLabel.text != ""){
                    cell.relationsTypeLabel.text = cell.relationsTypeLabel.text! + " / " + item
                }
                else{
                    cell.relationsTypeLabel.text = item
                }
            }

            return cell
            
            
        // Options
        }else if indexPath.section == 2 {
            if settingsOptionsItens[indexPath.row] == "Habilita Som"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "optionsCell", for: indexPath) as! SettingsOptionsCell
                cell.optionsChoiceSwitch.accessibilityIdentifier = settingsOptionsItens[indexPath.row]
                cell.optionsTitleLabel.text = settingsOptionsItens[indexPath.row]
                cell.optionsChoiceSwitch.setOn(Tools.box.settings.getPlaySplash(), animated: true)
                return cell
            }else if settingsOptionsItens[indexPath.row] == "Conta"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "buttonCell", for: indexPath) as! SettingsOptionsCell
                cell.optionsTitleLabel.text = settingsOptionsItens[indexPath.row]
                if Auth.auth().currentUser != nil {
                    cell.optionsButton.titleLabel?.text = "Remover"
                }else{
                    cell.optionsButton.titleLabel?.text = "Entrar"
                }
                return cell
            }else{
                return UITableViewCell()
            }
        }else{
            
            return UITableViewCell.init()
            
        }
        
        
    }
  /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
}
