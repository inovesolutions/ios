//
//  FirstViewController.swift
//  Olá Escola
//
//  Created by Rafael Marinheiro on 09/08/17.
//  Copyright © 2017 Bmtech Soluções Tecnológicas. All rights reserved.
//
import UIKit
import Firebase
import AVFoundation
import FontAwesome_swift

class MessagesTableViewCell: TDBadgedCell{

    @IBOutlet weak var TypeMessageLabel: UILabel!
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var SubtitleLabel: UILabel!
    @IBOutlet weak var BadgeLabel: UILabel!
    @IBOutlet weak var TimeLabel: UILabel!
    @IBOutlet weak var ModuleIconLabel: UILabel!

}


class MessagesTableViewController: UITableViewController, MessagesProtocol, AuthProtocol {

    @IBOutlet weak var FilterBarButtonItem: UIBarButtonItem!

    @IBAction func tapFilterBarButtonItem(_ sender: Any) {
    }
    
    var authHandle : AuthStateDidChangeListenerHandle? = nil
    
    // Auth Protocol
    func authRequire() { self.authenticationScreen() }
    
    // Messages Protocol
    func onAdded() {
        tableView.reloadData()
    }
    func onChanged() {
        tableView.reloadData()
    }
    func onRemoved() {
        tableView.reloadData()
    }
    
    // didLoad - Carrega para a variavel todo o banco.
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        
        if Auth.auth().currentUser == nil { self.authenticationScreen() }
        fb.db.authProtocol = self
        fb.db.messsageProtocol = self
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        /*
        // Monitora o estado da autenticação
        */
        tableView.reloadData()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(false)
    }
    
    
    
    // Alimenta as celulas
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let messages = fb.db.me.messages
        let message = messages[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "textCell", for: indexPath) as! MessagesTableViewCell
        
        let title = message.content.body.title
        let subtitle = message.content.body.subTitle
        let date = message.content.header.date
        
        cell.TitleLabel?.text = title
        cell.SubtitleLabel?.text = subtitle
        cell.TimeLabel.text = Tools.box.IsoToResumeDate(isoDate: date)
        
        if (message.read ?? false) {
            cell.backgroundColor = UIColor.white
        }else{
            cell.backgroundColor = UIColor.init(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
        }

        //var url = URL(string: message.content.header.senderAvatar)
        let placeHolderName : String = "IconeAvatarMOn"
        _ = UIImage(named: placeHolderName)!
        
        
        if message.content.header.moduleIcon == "" {
            cell.ModuleIconLabel.text = "?"
        }else{
            let unicodeIcon = Character(UnicodeScalar(UInt32(hexString: message.content.header.moduleIcon)!)!)
            cell.ModuleIconLabel.font = UIFont.fontAwesome(ofSize: 18)
            cell.ModuleIconLabel.text = "\(unicodeIcon) "
        }
         
        cell.ModuleIconLabel.backgroundColor = message.content.header.moduleIconBackground.getUIColor()
        cell.ModuleIconLabel.textColor = message.content.header.moduleIconColor.getUIColor()
        cell.ModuleIconLabel.textAlignment = .center
        cell.ModuleIconLabel.sizeThatFits(CGSize(width: 30.0, height: 30.0))
        cell.ModuleIconLabel.layer.cornerRadius = 15.0
        cell.ModuleIconLabel.layer.masksToBounds = true
        
        cell.TypeMessageLabel.font = UIFont.fontAwesome(ofSize: 16)
        cell.TypeMessageLabel.text = Tools.box.iconType(typeMessage: message.content.type)
        return cell
    }
    
    // diz que a celula pode ser editada/removida
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.delete
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let selectedIndex = self.tableView.indexPathForSelectedRow {
            let message = fb.db.me.messages[selectedIndex.row]
            if ( message.content.type == "image" ) {
                if let viewController = UIStoryboard(name: "Messages", bundle: nil).instantiateViewController(withIdentifier: "MessageImageViewController") as? MessageImageViewController {
                    viewController.message = fb.db.me.messages[selectedIndex.row]
                    if let navigator = navigationController {
                        navigator.pushViewController(viewController, animated: true)
                    }
                }
            } else if ( message.content.type == "text") {
                if let viewController = UIStoryboard(name: "Messages", bundle: nil).instantiateViewController(withIdentifier: "MessageTextViewController") as? MessageTextViewController {
                    viewController.message = fb.db.me.messages[selectedIndex.row]
                    if let navigator = navigationController {
                        navigator.pushViewController(viewController, animated: true)
                    }
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        fb.db.checkDeleteMessage(message: fb.db.me.messages[indexPath.row])
        fb.db.me.messages.remove(at: indexPath.row)
        self.tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.top)
    }

    override func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Apagar"
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fb.db.me.messages.count
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let selectedIndex = self.tableView.indexPathForSelectedRow {
            if segue.identifier == "htmltextCellSegue"{
                if let dest = segue.destination as? MessagesDetailViewController{
                        dest.message = fb.db.me.messages[selectedIndex.row]
                }
            }
        }
    }

}
