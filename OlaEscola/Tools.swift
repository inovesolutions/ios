//
//  Tools.swift
//  Olá Escola
//
//  Created by Rafael Marinheiro on 17/08/17.
//  Copyright © 2017 Bmtech Soluções Tecnológicas. All rights reserved.
//

import FontAwesome_swift
import AudioToolbox
import UIKit


class Tools {
    
    static let box = Tools()
    let settings = Settings()
    
    private init() {
    }
        
    func playAudio(wavFilename: String){
        play(sound: getSound(fileName: wavFilename))
    }
    func play1s(){
        play(sound: getSound(fileName: "SoundLogo1s"))
    }
    func play3s(){
        play(sound: getSound(fileName: "SoundLogo3s"))
    }
    func play5s(){
        play(sound: getSound(fileName: "SoundLogo5s"))
    }
    func play8s(){
        play(sound: getSound(fileName: "SoundLogo8s"))
    }

    private func getSound(fileName: String) -> SystemSoundID? {

        if let soundUrl = Bundle.main.url(forResource: fileName, withExtension: "wav"){
            var soundId : SystemSoundID = 0
            AudioServicesCreateSystemSoundID(soundUrl as CFURL, &soundId)
            AudioServicesAddSystemSoundCompletion(soundId, nil, nil, { (soundId, clientData) in
                AudioServicesDisposeSystemSoundID(soundId)
            }, nil)
            return soundId
        }else{
            return nil
        }
    }
    
    private func play(sound: SystemSoundID?){
        if sound != nil{
            AudioServicesPlaySystemSound(sound!)
        }
    }
    
    
    func NSDateToResumeDate(date :NSDate) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.short
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale(identifier: "pt_BR")
        return dateFormatter.string(from: date as Date)
    }
    
    func IntToResumeDate(timestamp :Int64) -> String{
        let date = Date(timeIntervalSince1970: TimeInterval(timestamp/1000))
        return timeAgoSince(date as Date)

    }
    
    func IntToDateOrTime(timestamp :Int64) -> String{
        let date = Date(timeIntervalSince1970: TimeInterval(timestamp/1000))
        return getDateOrTime(date as Date)
    }
    
    public func getDateOrTime(_ date: Date) -> String {
        
        let calendar = Calendar.current
        let now = Date()
        let unitFlags: NSCalendar.Unit = [.second, .minute, .hour, .day, .weekOfYear, .month, .year]
        let components = (calendar as NSCalendar).components(unitFlags, from: date, to: now, options: [])
        
        let dateFormatter = DateFormatter()

        if let day = components.day, day >= 2 {
            dateFormatter.dateFormat = "dd/MM"
            return dateFormatter.string(from: date)
        }
        
        if let day = components.day, day >= 1 {
            return "Ontem"
        }
        else{
            dateFormatter.dateFormat = "HH:mm"
            return dateFormatter.string(from: date)
        }
        
    }
    
    public func timeAgoSince(_ date: Date) -> String {
        
        let calendar = Calendar.current
        let now = Date()
        let unitFlags: NSCalendar.Unit = [.second, .minute, .hour, .day, .weekOfYear, .month, .year]
        let components = (calendar as NSCalendar).components(unitFlags, from: date, to: now, options: [])
        
        if let year = components.year, year >= 2 {
            return "\(year) anos atrás"
        }
        
        if let year = components.year, year >= 1 {
            return "Ano passado"
        }
        
        if let month = components.month, month >= 2 {
            return "\(month) meses atrás"
        }
        
        if let month = components.month, month >= 1 {
            return "Mês passado"
        }
        
        if let week = components.weekOfYear, week >= 2 {
            return "\(week) semanas atrás"
        }
        
        if let week = components.weekOfYear, week >= 1 {
            return "Semana passada"
        }
        
        if let day = components.day, day >= 2 {
            return "\(day) dias atrás"
        }
        
        if let day = components.day, day >= 1 {
            return "Ontem"
        }
        
        if let hour = components.hour, hour >= 2 {
            return "\(hour) horas atrás"
        }
        
        if let hour = components.hour, hour >= 1 {
            return "Uma hora atrás"
        }
        
        if let minute = components.minute, minute >= 2 {
            return "\(minute) minutos atrás"
        }
        
        if let minute = components.minute, minute >= 1 {
            return "Um minuto atrás"
        }
        
        if let second = components.second, second >= 3 {
            return "\(second) segundos atrás"
        }
        
        return "Agora"
        
    }
    
    func IsoToResumeDate(isoDate :String) -> String{

        // Converte a iso para data
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        if dateFormatter.date(from: isoDate) == nil {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            if dateFormatter.date(from: isoDate) == nil {
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm'Z'"
                if dateFormatter.date(from: isoDate) == nil {
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
                }
            }
        }
        
        if let date = dateFormatter.date(from: isoDate){
            return timeAgoSince(date)
        }else{
            return "..."
        }
    }
    
    func Alert(messageTitle:String, messageContent:String, viewController: UIViewController)
    {
        let myAlert = UIAlertController(title: messageTitle, message: messageContent, preferredStyle: UIAlertControllerStyle.alert);
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
        myAlert.addAction(okAction)
        viewController.present(myAlert, animated: true, completion: nil)
    }
    
    class Settings{
        
        func getNavBarBackgroundColor() -> UIColor {
            return UIColor.rbg(r: 220, g: 248, b: 198)
        }
        
        func getPlaySplash() -> Bool {
            if UserDefaults.standard.object(forKey: "Settings.PlaySound.SplashScreen") == nil {
                UserDefaults.standard.set(true, forKey: "Settings.PlaySound.SplashScreen")
                return true
            }else{
                return UserDefaults.standard.bool(forKey: "Settings.PlaySound.SplashScreen")
            }
        }

        func setPlaySplash(_ active: Bool ) {
            UserDefaults.standard.set(active, forKey: "Settings.PlaySound.SplashScreen")
        }
    }
    
    func iconType(typeMessage: String) -> String{
        if typeMessage == "image" {
            let unicodeIcon = Character(UnicodeScalar(UInt32(hexString: "f030")!)!)
            return "\(unicodeIcon)"
        }else if typeMessage == "video" {
            let unicodeIcon = Character(UnicodeScalar(UInt32(hexString: "f03d")!)!)
            return "\(unicodeIcon)"
        }else{
            return ""
        }
    }
    
    func setImage(_ url:String, placeholder:UIImage) ->UIImage {

        //verifica se o arquivo existe localmente, se existir devolve a imagem, caso contrario inicia o download
        
        var localFileUrl = FileManager.default.urls(for: .documentDirectory, in:.userDomainMask).first!
        localFileUrl.appendPathComponent("images", isDirectory: true)
        localFileUrl.appendPathComponent(url)
        
        if url == "" {
            return UIImage()
        }
        
        if (FileManager.default.fileExists(atPath: localFileUrl.path)) {
            let imageData = NSData(contentsOf: localFileUrl)
            return UIImage(data: imageData! as Data)!
        }else{
            return UIImage()
        }
    }
    
    
    // ARQUIVOS LOCAIS
    func avatarLocalFile(_ profile : Profile) -> URL?{
        
        var localFileUrl = FileManager.default.urls(for: .documentDirectory, in:.userDomainMask).first!
        localFileUrl.appendPathComponent("avatars", isDirectory: true)
        localFileUrl.appendPathComponent(profile.profileId , isDirectory: true)
        localFileUrl.appendPathComponent(profile.avatar)
        
        if (FileManager.default.fileExists(atPath: localFileUrl.path)) {
            return localFileUrl
        }else{
            return nil
        }
    }
    
}

