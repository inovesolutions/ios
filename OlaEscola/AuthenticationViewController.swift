//
//  AuthenticationViewController.swift
//  Olá Escola
//
//  Created by Rafael Marinheiro on 13/08/17.
//  Copyright © 2017 Bmtech Soluções Tecnológicas. All rights reserved.
//


import UIKit
import Firebase
import SwiftSpinner

class AuthenticationViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var txtMobileNumber: UITextField!
    @IBOutlet weak var StackViewSmsCode: UIStackView!
    @IBOutlet weak var LabelViewMensagem: UILabel!
    @IBOutlet weak var ImageViewIcone: UIImageView!
    @IBOutlet weak var ButtonGetCode: UIButton!
    
    @IBOutlet weak var TextViewSmsCode1: UITextView!
    @IBOutlet weak var TextViewSmsCode2: UITextView!
    @IBOutlet weak var TextViewSmsCode3: UITextView!
    @IBOutlet weak var TextViewSmsCode4: UITextView!
    @IBOutlet weak var TextViewSmsCode5: UITextView!
    @IBOutlet weak var TextViewSmsCode6: UITextView!
    
    @IBAction func getCode(_ sender: Any) {
        showPhone()
    }
    
    
    @IBAction func txtMobileNumberClick(_ sender: Any) {
        
        if txtMobileNumber.text == nil
        {
            self.alert(message: "Você esqueceu de preencher o número de telefone com ddd!", title: "Ops...")
            return
        }
        
        let phoneNumber = txtMobileNumber.text!
        print(phoneNumber)
        
        
        if ( (txtMobileNumber.text!.count) >= 10 && (txtMobileNumber.text!.characters.count) <= 13 ){
            
            // Custom token
            let dict = ["phoneNumber": "\(txtMobileNumber.text!)"] as [String: Any]
            if let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted) {
                
                
                let url = NSURL(string: "https://us-central1-colegiouirapuru-b9be5.cloudfunctions.net/verifyPhoneNumber")!
                let request = NSMutableURLRequest(url: url as URL)
                
                request.httpMethod = "POST"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.httpBody = jsonData
                
            
                let task = URLSession.shared.dataTask(with: request as URLRequest){ data,response,error in

                    SwiftSpinner.hide()
                    if error != nil{
                        print(error?.localizedDescription ?? "Erro nao conhecido")
                        return
                    }
                    
                    do {

                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        if let parseJSON = json {
                            if let resultValue:String = parseJSON["credentials"] as? String {
                                //sucesso
                                UserDefaults.standard.set(resultValue, forKey: "authVerificationCredentials")
                                DispatchQueue.main.async() {
                                    self.showSms()
                                }

                            }
                            if let resultValue:String = parseJSON["reason"] as? String {
                                //erro
                                DispatchQueue.main.async() {
                                    self.alert(message: resultValue, title: "Ops...")
                                }
                                print(resultValue)
                            }
                            
                        }else{
                            DispatchQueue.main.async() {
                                self.alert(message: "Houve um erro ao se comunicar com o servidor, certifique-se que você está com acesso a internet.", title: "Ops...")
                            }
                        }
                    } catch let error as NSError {
                        print(error)
                    }
                }          
                task.resume()
                SwiftSpinner.show("Aguarde enquanto verificamos as informações...")
            }
            
        }else{
            self.alert(message: "É necessário preencher o codigo de área com 2 digitos e o numero do seu celular.", title: "Verifique o número")
        }
    }
    
    

    func showPhone(){
        self.ButtonGetCode.isHidden = true
        self.txtMobileNumber.isHidden = false
        self.StackViewSmsCode.isHidden = true
        self.LabelViewMensagem.text = "Para iniciar você precisa informar seu número de celular com DDD."
        self.ImageViewIcone.image = #imageLiteral(resourceName: "IconeMobileOn")
    }

    func showSms(){
        self.txtMobileNumber.isHidden = true
        self.StackViewSmsCode.isHidden = false
        self.LabelViewMensagem.text = "você receberá o código de verificação por SMS."
        self.ImageViewIcone.image = #imageLiteral(resourceName: "IconeSmsOn")
        self.ButtonGetCode.isHidden = false
        self.TextViewSmsCode1.becomeFirstResponder()
        self.TextViewSmsCode1.text = ""
        self.TextViewSmsCode2.text = ""
        self.TextViewSmsCode3.text = ""
        self.TextViewSmsCode4.text = ""
        self.TextViewSmsCode5.text = ""
        self.TextViewSmsCode6.text = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Cria o botao do campo do celular
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "IconePlayOn"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        button.frame = CGRect(x: CGFloat(txtMobileNumber.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        button.addTarget(self, action: #selector(self.txtMobileNumberClick), for: .touchUpInside)
        txtMobileNumber.rightView = button
        txtMobileNumber.rightViewMode = .always
        
        // Associa os objetos para dar focu
        txtMobileNumber.delegate = self
        TextViewSmsCode1.delegate = self
        TextViewSmsCode2.delegate = self
        TextViewSmsCode3.delegate = self
        TextViewSmsCode4.delegate = self
        TextViewSmsCode5.delegate = self
        TextViewSmsCode6.delegate = self
        
    }


    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        
        // verificar se possui todos os codigos digitados
        
        let charsAllowed = "0123456789"
        
        let c1:String = self.TextViewSmsCode1.text
        let c2:String = self.TextViewSmsCode2.text
        let c3:String = self.TextViewSmsCode3.text
        let c4:String = self.TextViewSmsCode4.text
        let c5:String = self.TextViewSmsCode5.text
        let c6:String = self.TextViewSmsCode6.text

        if text == "" {
            // Backspace apertado
            if c1.count == 0 {
                self.TextViewSmsCode1.becomeFirstResponder()
            }else if c2.count == 0 {
                self.TextViewSmsCode1.becomeFirstResponder()
            }else if c3.count == 0 {
                self.TextViewSmsCode2.becomeFirstResponder()
            }else if c4.count == 0 {
                self.TextViewSmsCode3.becomeFirstResponder()
            }else if c5.count == 0 {
                self.TextViewSmsCode4.becomeFirstResponder()
            }else if c6.count == 0 {
                self.TextViewSmsCode5.becomeFirstResponder()
            }else{
                self.TextViewSmsCode6.becomeFirstResponder()
            }
            return true
        }else if charsAllowed.range(of: text) != nil {
            // Numero digitado
            if c1.count == 0 {
                self.TextViewSmsCode1.becomeFirstResponder()
            }else if c2.count == 0 {
                self.TextViewSmsCode2.becomeFirstResponder()
            }else if c3.count == 0 {
                self.TextViewSmsCode3.becomeFirstResponder()
            }else if c4.count == 0 {
                self.TextViewSmsCode4.becomeFirstResponder()
            }else if c5.count == 0 {
                self.TextViewSmsCode5.becomeFirstResponder()
            }else if c6.count == 0 {
                self.TextViewSmsCode6.becomeFirstResponder()
                TextViewSmsCode6.text = text
                self.TextViewSmsCode6.resignFirstResponder()
                let smsCode:String = c1 + c2 + c3 + c4 + c5 + text
                
                // Manda processar o codigo de sms
                let verificationCredentials = UserDefaults.standard.string(forKey: "authVerificationCredentials")
                
                
                let dict = ["code": "\(smsCode)", "credentials":"\(verificationCredentials ?? "")"] as [String: Any]
                if let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted) {
                    
                    let url = NSURL(string: NSLocalizedString("https://us-central1-colegiouirapuru-b9be5.cloudfunctions.net/verifyWithCredentials", comment: ""))!
                    let request = NSMutableURLRequest(url: url as URL)
                    
                    request.httpMethod = "POST"
                    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.httpBody = jsonData
                    
                    
                    let task = URLSession.shared.dataTask(with: request as URLRequest){ data,response,error in
                        
                        SwiftSpinner.hide()
                        if error != nil{
                            print(error?.localizedDescription ?? "Erro nao conhecido")
                            return
                        }
                        
                        do {
                            
                            let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                            
                            if let parseJSON = json {
                                if let resultValue:String = parseJSON["token"] as? String {
                                    //sucesso
                                    Auth.auth().signIn(withCustomToken: resultValue, completion: { (userAuth, errorAuth) in
                                        if errorAuth == nil {
                                            UserDefaults.standard.removeObject(forKey: "authVerificationCredentials")
                                            fb.db.clearMe()
                                            fb.db.connect()
                                            self.dismiss(animated: true, completion: nil)
                                        }else{
                                            DispatchQueue.main.async() {
                                                self.alert(message: "O código informado não é válido. Verifique o código digitado ou solicite um novo código.", title: "Ops...")
                                                print("FIREBASEAUTH signIn :", errorAuth!.localizedDescription)
                                            }
                                        }
                                        
                                    })
                                }
                                if let resultValue:String = parseJSON["reason"] as? String {
                                    //erro
                                    DispatchQueue.main.async() {
                                        self.alert(message: resultValue, title: "Ops...")
                                    }
                                }
                                
                            }else{
                                DispatchQueue.main.async() {
                                    self.alert(message: "Houve um erro ao se comunicar com o servidor, certifique-se que você está com acesso a internet.", title: "Ops...")
                                }
                            }
                        } catch let error as NSError {
                            print(error)
                        }        
                    }          
                    task.resume()
                    SwiftSpinner.show("Verificando o código informado...")
                }
                
            }else{
                self.TextViewSmsCode6.resignFirstResponder()
                self.alert(message: "Enviei o código, estou aguardando me responderem para eu continuar, aguarde um instante.", title: "Em processamento")
                return false
            }
            return true
        }
    
        return false
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
