//
//  MessageImageViewController.swift
//  OlaEscola
//
//  Created by Developer on 19/01/2018.
//  Copyright © 2018 Bmtech Soluções Tecnológicas. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class MessageImageViewController: UITableViewController, StorageImageDownloadProtocol, AuthProtocol {
    
    func authRequire() { self.authenticationScreen() }
    var message : Message = Message()
    var sectionsNames : Array<String?> = ["","Relacionado à"]
    var messageImageView: UIImageView = UIImageView()

    // Storage Protocol
    func onSuccess(){
         var localFileUrl = FileManager.default.urls(for: .documentDirectory, in:.userDomainMask).first!
        localFileUrl.appendPathComponent("images", isDirectory: true)
        localFileUrl.appendPathComponent(message.content.body.content)
        let imageData = NSData(contentsOf: localFileUrl)
        self.messageImageView.image = UIImage(data: imageData! as Data)
        
        if self.message.read! == false {
            fb.db.checkReadMessage(message: self.message)
        }
    }
    func onFail(error: Error){
        
        let alert = UIAlertController(title: "Ops...", message: "Não possível baixar a mensagem agora, por favor tente mais tarde", preferredStyle: UIAlertControllerStyle.alert)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func onPause(){
    }
    
    func onResume(){
    }
    
    func onProgress(completed: Float){

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fb.db.authProtocol = self
        fb.db.storageImageDownloadProtocol = self
        self.tableView.rowHeight = UITableViewAutomaticDimension
        tableView.reloadData()
        self.tableView.tableFooterView = UIView()
        self.title = ""
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.sectionsNames.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.message.content.header.relations.count > 0 {
            return sectionsNames[section]
        }else{
            var tempSections = sectionsNames
            tempSections[1] = nil
            return tempSections[section]
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 { // Content
            return 1
        }else if section == 1 { // Relations
            if self.message.content.header.relations.count > 0 {
                return self.message.content.header.relations.count
            }
        }
        
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "contentCell", for: indexPath) as! ImageContentCell
            cell.moduleIconLabel.text = Tools.box.iconType(typeMessage: message.content.type)
            cell.TitleLabel.text = self.message.content.body.title
            cell.SubtitleLabel.text = self.message.content.body.subTitle
            cell.senderNameLabel.text = self.message.content.header.senderName
            cell.imageSubtitleLabel.attributedText = self.message.content.body.imageDescription?.html2AttributedString
            
            let senderProfile = Profile()
            senderProfile.profileId = self.message.content.header.senderId
            senderProfile.avatar = self.message.content.header.senderAvatar
            
            cell.senderAvatarImageView.image = UIImage.init(named: "IconeAvatar")
            
            fb.db.storageGetImageAvatar(senderProfile){ (image) -> () in
                cell.senderAvatarImageView.image = image
            }
            
            cell.senderAvatarImageView.layer.borderWidth = 2.0
            cell.senderAvatarImageView.layer.borderColor = UIColor.lightGray.cgColor
            cell.senderAvatarImageView.layer.cornerRadius = cell.senderAvatarImageView.frame.size.width / 2
            cell.senderAvatarImageView.clipsToBounds = true
            if message.content.header.moduleIcon == "" {
                cell.moduleIconLabel.text = "?"
            }else{
                let unicodeIcon = Character(UnicodeScalar(UInt32(hexString: message.content.header.moduleIcon)!)!)
                cell.moduleIconLabel.font = UIFont.fontAwesome(ofSize: 18)
                cell.moduleIconLabel.text = "\(unicodeIcon) "
            }
            
            cell.moduleIconLabel.backgroundColor = message.content.header.moduleIconBackground.getUIColor()
            cell.moduleIconLabel.textColor = message.content.header.moduleIconColor.getUIColor()
            cell.moduleIconLabel.textAlignment = .center
            cell.moduleIconLabel.sizeThatFits(CGSize(width: 30.0, height: 30.0))
            cell.moduleIconLabel.layer.cornerRadius = 15.0
            cell.moduleIconLabel.layer.masksToBounds = true

            cell.timeLabel.text = Tools.box.IsoToResumeDate(isoDate: self.message.content.header.date)
            
            self.messageImageView = cell.messageImageView
            self.messageImageView.contentMode = UIViewContentMode.scaleAspectFit
            self.messageImageView.isUserInteractionEnabled = true
            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
            self.messageImageView.addGestureRecognizer(tapRecognizer)
            var markReadMessage = false
            
            fb.db.storageImageDownloadProtocol = self
            
            // verifica se o arquivo já foi baixado
            var localFileUrl = FileManager.default.urls(for: .documentDirectory, in:.userDomainMask).first!
            localFileUrl.appendPathComponent("images", isDirectory: true)
            localFileUrl.appendPathComponent(message.content.body.content)
            
            if (FileManager.default.fileExists(atPath: localFileUrl.path)) {
                let imageData = NSData(contentsOf: localFileUrl)
                self.messageImageView.image = UIImage(data: imageData! as Data)
                markReadMessage = true
            }else{
                fb.db.storageGetImage(message.content.body.content)
            }
            
            if markReadMessage {
                if self.message.read! == false {
                    fb.db.checkReadMessage(message: self.message)
                }
            }

            return cell
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "relationsCell", for: indexPath) as! RelationsCell
            let relationProfile = self.message.content.header.relations[indexPath.row].profile
            
            var iconAvatar = ""
            if(relationProfile.gender == "male"){
                iconAvatar = "IconeAvatarMOn"
            }
            else {
                iconAvatar = "IconeAvatarFOn"
            }
            cell.relationAvatarImageView.image = UIImage.init(named: iconAvatar)
            fb.db.storageGetImageAvatar(relationProfile){ (image) -> () in
                cell.relationAvatarImageView.image = image
            }
            cell.relationAvatarImageView.layer.borderWidth = 2.0
            cell.relationAvatarImageView.layer.borderColor = UIColor.white.cgColor
            cell.relationAvatarImageView.layer.cornerRadius = cell.relationAvatarImageView.frame.size.width / 2
            cell.relationAvatarImageView.clipsToBounds = true
            cell.relationNameLabel.text = relationProfile.displayName
            
            return cell
            
        }
        
    }
    
    @objc func imageTapped(recognizer: UITapGestureRecognizer){
        if let viewController = UIStoryboard(name: "Messages", bundle: nil).instantiateViewController(withIdentifier: "MessageImageFullScreenViewController") as? MessageImageFullScreenViewController {
            viewController.message = self.message
            self.present(viewController, animated: true)
        }
    }
    
}
