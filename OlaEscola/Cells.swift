//
//  Cells.swift
//  OlaEscola
//
//  Created by Developer on 19/01/2018.
//  Copyright © 2018 Bmtech Soluções Tecnológicas. All rights reserved.
//

import Foundation
import UIKit

class ImageContentCell: UITableViewCell{
    @IBOutlet weak var senderAvatarImageView: UIImageView!
    @IBOutlet weak var senderNameLabel: UILabel!
    @IBOutlet weak var moduleIconLabel: UILabel!
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var SubtitleLabel: UILabel!
    @IBOutlet weak var imageSubtitleLabel: UILabel!
    @IBOutlet weak var messageImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
}

class TextContentCell: UITableViewCell{
    @IBOutlet weak var senderAvatarImageView: UIImageView!
    @IBOutlet weak var senderNameLabel: UILabel!
    @IBOutlet weak var moduleIconLabel: UILabel!
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var SubtitleLabel: UILabel!
    @IBOutlet weak var htmlTextLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
}

class RelationsCell: UITableViewCell {
    @IBOutlet weak var relationAvatarImageView: UIImageView!
    @IBOutlet weak var relationNameLabel: UILabel!
}
