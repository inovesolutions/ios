//
//  MessagesDetailViewController.swift
//  Olá Escola
//
//  Created by Rafael Marinheiro on 22/08/17.
//  Copyright © 2017 Bmtech Soluções Tecnológicas. All rights reserved.
//

import UIKit

class MessagesDetailViewController: UIViewController, StorageImageDownloadProtocol, AuthProtocol {

    // Auth Protocol
    func authRequire() { self.authenticationScreen() }
    
    var message : Message = Message()
    
    @IBOutlet weak var htmltextTextView: UITextView!
    @IBOutlet weak var imageImageView: UIImageView!
    @IBOutlet weak var downloadProgressView: UIProgressView!
    @IBOutlet weak var downloadLabel: UILabel!
    @IBOutlet weak var photoIconeLabel: UILabel!
    
    // Storage Protocol
    func onSuccess(){
        var localFileUrl = FileManager.default.urls(for: .documentDirectory, in:.userDomainMask).first!
        localFileUrl.appendPathComponent("images", isDirectory: true)
        localFileUrl.appendPathComponent(message.content.body.content)
        let imageData = NSData(contentsOf: localFileUrl)
        self.imageImageView.image = UIImage(data: imageData! as Data)
        imageImageView.isHidden = false
    }
    func onFail(error: Error){
        
        // create the alert
        let alert = UIAlertController(title: "Ops...", message: "Não foi possível baixar a imagem, verifique se sua conexão com a intenet está ok e tente novamente. Caso o erro persista, informe o suporte para encontrarem o que está errado e solucionar este problema.", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Tentar novamente", style: UIAlertActionStyle.default, handler: {
            action in
            fb.db.storageGetImage(self.message.content.body.content)
            self.downloadProgressView.isHidden = false
            self.downloadProgressView.progress = 0.0
            self.downloadLabel.isHidden = false
            self.photoIconeLabel.isHidden = false

        }))

        alert.addAction(UIAlertAction(title: "Depois eu vejo", style: UIAlertActionStyle.cancel, handler: {
            action in
            self.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    func onPause(){
        downloadLabel.text = " pausado! "
    }
    func onResume(){
            downloadLabel.text = " retomando download! "
    }

    func onProgress(completed: Float){
        if completed == 100.0 {
            downloadProgressView.isHidden = true
            downloadLabel.isHidden = true
            photoIconeLabel.isHidden = true
        }else{
            self.downloadProgressView.progress = completed
            downloadLabel.text = " \(completed)% concluído"
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Carrega o protocolo Auth
        fb.db.authProtocol = self
        
        imageImageView.isHidden = true
        htmltextTextView.isHidden = true
        downloadLabel.isHidden = true
        downloadProgressView.isHidden = true
        photoIconeLabel.isHidden = true
        photoIconeLabel.text = Tools.box.iconType(typeMessage: message.content.type)
        var markReadMessage = false
        
        if message.content.type == "image" {

            imageImageView.isHidden = false
            
            // Carrega o protocolo
            fb.db.storageImageDownloadProtocol = self
            
            self.title = message.content.body.title
            
            // verifica se o arquivo já foi baixado
            var localFileUrl = FileManager.default.urls(for: .documentDirectory, in:.userDomainMask).first!
            localFileUrl.appendPathComponent("images", isDirectory: true)
            localFileUrl.appendPathComponent(message.content.body.content)
            
            if (FileManager.default.fileExists(atPath: localFileUrl.path)) {
                let imageData = NSData(contentsOf: localFileUrl)
                self.imageImageView.image = UIImage(data: imageData! as Data)
                markReadMessage = true
            }else{
                fb.db.storageGetImage(message.content.body.content)
                downloadProgressView.isHidden = false
                downloadLabel.isHidden = false
                photoIconeLabel.isHidden = false
            }

        }else if message.content.type == "text" {
            htmltextTextView.isHidden = false
            self.htmltextTextView.attributedText = self.message.content.body.content.html2AttributedString
            markReadMessage = true
        }
        
        if markReadMessage {
            if message.read! == false {
                fb.db.checkReadMessage(message: message)
            }
            
        }
        
        //self.title = message.content.body.title
        //self.TitleLabel.text = message!.title
        //self.SubTitleLabel.text = message!.subtitle
        //self.TitleLabel.sizeToFit()
        //self.SubTitleLabel.sizeToFit()
        //self.ContentLabel.sizeToFit()
    
    }
    
    override func viewDidLayoutSubviews() {
        // Posicionar o cursor no inicio da textview
        self.htmltextTextView.setContentOffset(CGPoint.zero, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
